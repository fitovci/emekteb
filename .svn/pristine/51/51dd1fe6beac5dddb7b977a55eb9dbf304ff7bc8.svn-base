using System.Data.Entity;
using eMektebRealForSure.Models;

namespace eMektebRealForSure.DAL
{
    public class MojContext : DbContext
    {
        public MojContext()
            : base("name=Model")
        {
        }

        public virtual DbSet<Edukator> Edukators { get; set; }
        public virtual DbSet<EvidencijaNapredovanja> EvidencijaNapredovanjas { get; set; }
        public virtual DbSet<Galerija> Galerijas { get; set; }
        public virtual DbSet<KalendarskaAktivnost> KalendarskaAktivnosts { get; set; }
        public virtual DbSet<Lekcija> Lekcijas { get; set; }
        public virtual DbSet<Medzlis> Medzlis { get; set; }
        public virtual DbSet<Mekteb> Mektebs { get; set; }
        public virtual DbSet<MjesecniPlan> MjesecniPlans { get; set; }
        public virtual DbSet<Muftijstvo> Muftijstvoes { get; set; }
        public virtual DbSet<Nastava> Nastavas { get; set; }
        public virtual DbSet<NastavniPlan> NastavniPlans { get; set; }
        public virtual DbSet<Osoba> Osobas { get; set; }
        public virtual DbSet<Polaznik> Polazniks { get; set; }
        public virtual DbSet<Poruke> Porukes { get; set; }
        public virtual DbSet<Razred> Razreds { get; set; }
        public virtual DbSet<RoditeljskiSastanak> RoditeljskiSastanaks { get; set; }
        public virtual DbSet<SkolskaGodina> SkolskaGodinas { get; set; }
        public virtual DbSet<SlikaInfo> SlikaInfoes { get; set; }
        public virtual DbSet<SluzbenaZabiljeska> SluzbenaZabiljeskas { get; set; }
        public virtual DbSet<Stepen> Stepens { get; set; }
        public virtual DbSet<Ucenik> Uceniks { get; set; }
        public virtual DbSet<Zadaca> Zadacas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Galerija>()
                .HasMany(e => e.SlikaInfoes)
                .WithRequired(e => e.Galerija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lekcija>()
                .HasMany(e => e.EvidencijaNapredovanjas)
                .WithRequired(e => e.Lekcija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lekcija>()
                .HasMany(e => e.Nastavas)
                .WithRequired(e => e.Lekcija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lekcija>()
                .HasMany(e => e.Zadacas)
                .WithRequired(e => e.Lekcija)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lekcija>()
                .HasMany(e => e.MjesecniPlans)
                .WithMany(e => e.Lekcijas)
                .Map(m => m.ToTable("MjesecniPlanLekcijas"));

            modelBuilder.Entity<Lekcija>()
                .HasMany(e => e.NastavniPlans)
                .WithMany(e => e.Lekcijas)
                .Map(m => m.ToTable("NastavniPlanLekcijas"));

            modelBuilder.Entity<Medzlis>()
                .HasMany(e => e.Mektebs)
                .WithRequired(e => e.Medzli)
                .HasForeignKey(e => e.MedzlisId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Mekteb>()
                .HasMany(e => e.Edukators)
                .WithOptional(e => e.Mekteb)
                .HasForeignKey(e => e.MektebId);

            modelBuilder.Entity<Mekteb>()
                .HasMany(e => e.RoditeljskiSastanaks)
                .WithRequired(e => e.Mekteb)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Mekteb>()
                .HasMany(e => e.SluzbenaZabiljeskas)
                .WithRequired(e => e.Mekteb)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Muftijstvo>()
                .HasMany(e => e.Medzlis)
                .WithRequired(e => e.Muftijstvo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nastava>()
                .HasMany(e => e.EvidencijaNapredovanjas)
                .WithRequired(e => e.Nastava)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nastava>()
                .HasMany(e => e.Zadacas)
                .WithRequired(e => e.Nastava)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nastava>()
                .HasMany(e => e.Polazniks)
                .WithMany(e => e.Nastavas)
                .Map(m => m.ToTable("UcenikNastavas").MapRightKey("Ucenik_Id"));

            modelBuilder.Entity<NastavniPlan>()
                .HasMany(e => e.Razreds)
                .WithRequired(e => e.NastavniPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Osoba>()
                .HasOptional(e => e.Edukator)
                .WithRequired(e => e.Osoba);

            modelBuilder.Entity<Osoba>()
                .HasMany(e => e.Porukes)
                .WithOptional(e => e.Primaoc)
                .HasForeignKey(e => e.PrimaocId);

            modelBuilder.Entity<Osoba>()
                .HasMany(e => e.Porukes1)
                .WithOptional(e => e.Posiljaoc)
                .HasForeignKey(e => e.PosiljaocId);

            modelBuilder.Entity<Osoba>()
                .HasOptional(e => e.Ucenik)
                .WithRequired(e => e.Osoba);

            modelBuilder.Entity<Polaznik>()
                .Property(e => e.FinalnaOcjena)
                .IsFixedLength();

            modelBuilder.Entity<Polaznik>()
                .Property(e => e.Komentar)
                .IsFixedLength();

            modelBuilder.Entity<Polaznik>()
                .Property(e => e.Vladanje)
                .IsFixedLength();

            modelBuilder.Entity<Polaznik>()
                .HasMany(e => e.EvidencijaNapredovanjas)
                .WithRequired(e => e.Polaznik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Polaznik>()
                .HasMany(e => e.Zadacas)
                .WithRequired(e => e.Polaznik)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Razred>()
                .HasMany(e => e.Galerijas)
                .WithRequired(e => e.Razred)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Razred>()
                .HasMany(e => e.MjesecniPlans)
                .WithRequired(e => e.Razred)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Razred>()
                .HasMany(e => e.Nastavas)
                .WithRequired(e => e.Razred)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Razred>()
                .HasMany(e => e.Polazniks)
                .WithRequired(e => e.Razred)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SkolskaGodina>()
                .HasMany(e => e.KalendarskaAktivnosts)
                .WithRequired(e => e.SkolskaGodina)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ucenik>()
                .HasMany(e => e.Polazniks)
                .WithRequired(e => e.Ucenik)
                .WillCascadeOnDelete(false);
        }

    }
}
