﻿
var mektebApp = angular.module("mektebApp", [
    'ngRoute',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngMessages', 
    'ngAnimate',
    'n3-pie-chart',
    'countTo'
]);

mektebApp.constant("Config", {
    source: "http://localhost:5757/api/"
});

mektebApp.run(function ($rootScope, $location, localStorageService) {
    var loggedUser = localStorageService.get('authorizationData');
    if (!loggedUser || loggedUser === null || loggedUser === undefined) {
        $location.path('/administracijalogin');
    }

    $rootScope.activeURL = $location;
});

mektebApp.config(function ($routeProvider) {
    $routeProvider
        .when("/administracijalogin", {
            templateUrl: "views/loginView.html",
            controller: "loginController"
        })
        .when("/dashboard", {
            templateUrl: "views/dashboardView.html",
            controller: "dashboardController"
        })
        .when("/mektebi", {
            templateUrl: "views/mektebView.html",
            controller: "mektebController"
        })
        .when("/edukatori", {
            templateUrl: "views/edukatorView.html",
            controller: "edukatorController"
        })
        .when("/aktivnosti", {
            templateUrl: "views/aktivnostiView.html",
            controller: "aktivnostController"
        })
        .when("/mektebskegodine", {
            templateUrl: "views/mektebskaGodinaView.html",
            controller: "mektebskaGodinaController"
        })
        .when("/nastavniplan", {
            templateUrl: "views/nastavniPlanView.html",
            controller: "nastavniPlanController"
        })
        .when("/nastavniplan/:id", {
            templateUrl: "views/manageNastavniPlanView.html",
            controller: "manageNastavniPlanController"
        })
        .when("/lekcije", {
            templateUrl: "views/lekcijeView.html",
            controller: "lekcijeController"
        })
        .when("/profile", {
            templateUrl: "views/profileView.html",
            controller: "profileController"
        })
        .when("/poruke", {
            templateUrl: "views/porukeView.html",
            controller: "porukeController"
        })
        .when("/home", {
            templateUrl: "views/homeView.html",
            controller: "homeController"
        })
        .otherwise({ redirectTo: "/home" });
});
