﻿(function () {
    "use strict";

    mektebApp.factory("manageNastavniPlanServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Lekcijas/";

        var getAll = function (nastavniPlanId) {
            return $http.get(Config.source + sourceExtension + 'GetLekcijeByPlan/' + nastavniPlanId);
        };

        var getById = function (id) {
            return $http.get(Config.source + sourceExtension + id);
        };

        var getLekcijeByRazredByPlan = function (razred, nastavniPlanId) {
            return $http.get(Config.source + sourceExtension + 'GetLekcije/'+ razred + '/' + nastavniPlanId);
        };

        var addLekcije = function (planId, items) {
            return $http.post(Config.source + sourceExtension + "AddLekcijaToPlan/" + planId, items);
        };

        var remove = function (planId, items) {
            return $http.post(Config.source + sourceExtension + "DeleteLekcijaFromPlan/" + planId, items);
        };

        return {
            getAll: getAll,
            getById: getById,
            getLekcijeByRazredByPlan: getLekcijeByRazredByPlan,
            addLekcije: addLekcije,
            remove: remove
        };

    }]);

})();