﻿(function () {
    "use strict";

    mektebApp.factory("porukeServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Porukes/";

        var getAllConversations = function (osobaId) {
            return $http.get(Config.source + sourceExtension + 'GetAllConversations/' + osobaId);
        };

        var getMessagesFromConversation = function (osoba1Id, osoba2Id) {
            return $http.get(Config.source + sourceExtension + 'GetMessagesFromConversation/' + osoba1Id + '/' + osoba2Id);
        };

        var post = function (object) {
            return $http.post(Config.source + sourceExtension, object);
        };

        var getOsobasForConversation = function () {
            return $http.get(Config.source + 'Osobas/GetOsobasForConversation');
        };

        return {
            getAllConversations: getAllConversations,
            getMessagesFromConversation : getMessagesFromConversation,
            post: post,
            getOsobasForConversation: getOsobasForConversation
        };

    }]);

})();