﻿(function () {
    "use strict";

    mektebApp.factory("statisticsServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Statistic/";

        var getStatistics = function () {
            return $http.get(Config.source + sourceExtension + "GetAktivnostiStatistics");
        };
        
        var getCounters = function () { 
            return $http.get(Config.source + sourceExtension + "GetCounters");
         };

        return {
            getAll: getStatistics,
            getCounters:getCounters
        };

    }]);

})();