﻿(function () {
    "use strict";

    mektebApp.factory("loginServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Login/";

        var post = function (object) {
            return $http.post(Config.source + sourceExtension, object);
        };

        return {
            post: post
        };

    }]);

})();