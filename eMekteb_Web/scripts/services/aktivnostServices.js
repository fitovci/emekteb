﻿(function () {
    "use strict";

    mektebApp.factory("aktivnostServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "KalendarskaAktivnosts/";

        var getAll = function () {
            return $http.get(Config.source + sourceExtension);
        };

        var getById = function (id) {
            return $http.get(Config.source + sourceExtension + id);
        }

        var post = function (object) {
            return $http.post(Config.source + sourceExtension, object);
        };

        var put = function (id, object) {
            return $http.put(Config.source + sourceExtension + id, object);
        };

        var remove = function(id) {
            return $http.get(Config.source + sourceExtension + "delete/" + id);
        };

        var getByGodina = function(godina, isCurrent) {
            return $http.get(Config.source + sourceExtension + "GetAktivnostsByGodina/" + godina + "/" + isCurrent);
        };

        return {
            getAll: getAll,
            getById: getById,
            post: post,
            put: put,
            remove: remove,
            getByGodina: getByGodina
        };

    }]);

})();