﻿(function () {
    "use strict";

    mektebApp.factory("lekcijeServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Lekcijas/";

        var getAll = function (razredId) {
            return $http.get(Config.source + sourceExtension + 'GetByRazred/' + razredId);
        };

        var getById = function (id) {
            return $http.get(Config.source + sourceExtension + id);
        }

        var post = function (object) {
            return $http.post(Config.source + sourceExtension, object);
        };

        var put = function (id, object) {
            return $http.put(Config.source + sourceExtension + id, object);
        };

        var remove = function (id) {
            return $http.delete(Config.source + sourceExtension + id);
        }

        return {
            getAll: getAll,
            getById: getById,
            post: post,
            put: put,
            remove: remove
        };

    }]);

})();