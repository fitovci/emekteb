﻿
mektebApp.directive('ngConfirmClick', ['$uibModal', '$timeout', function ($uibModal, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/confirmModalView.html',
                    controller: 'confirmModalController',
                    size: 'sm',
                    backdrop: 'static',
                    resolve: {
                        message: function () {
                            return attrs.ngConfirmMessage;
                        }
                    }
                });


                modalInstance.result.then(
                    function (action) {
                        if (action) {
                            $timeout(function () {
                                scope.$apply(attrs.ngConfirmClick);
                            });
                        }
                    }
                );
            });
        }
    }
}]);