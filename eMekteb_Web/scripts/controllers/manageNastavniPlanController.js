﻿(function () {

    mektebApp.controller("manageNastavniPlanController", function ($scope, $routeParams, localStorageService, manageNastavniPlanServices, nastavniPlanServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        $scope.isLoading2 = false;
        $scope.selectedItems = [];
        $scope.razredi = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        $scope.selectedRazred = $scope.razredi[0];

        var services = manageNastavniPlanServices;
        var nastavniPlanId = $routeParams.id;


        //CRUD functions
        var getAll = function (nastavniPlanId) {
            $scope.isLoading = true;
            services.getAll(nastavniPlanId).then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lekcije nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mekteb nije učitan!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll(nastavniPlanId);
                   notificationsConfig.success("Novi mekteb je uspješno dodan!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Novi mekteb nije uspješno dodan!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll(nastavniPlanId);
                   notificationsConfig.success("Mekteb je uspješno uređen!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Mekteb nije uspješno uređen!");
               }
           );
        };

        $scope.remove = function () {
            services.remove(nastavniPlanId, $scope.selectedItems).then(
                function (response) {
                    getAll(nastavniPlanId);
                    $scope.selectedItems = [];
                    notificationsConfig.success("Lekcije su uspješno uklonjene iz nastavnog plana!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Lekcije nisu uspješno uklonjene iz nastavnog plana!");
               }
           );
        };

        $scope.addLekcijeToPlan = function () {
            services.addLekcije(nastavniPlanId, $scope.selectedItems).then(
                function (response) {
                    getAll(nastavniPlanId);
                    $scope.selectedItems = [];
                    notificationsConfig.success("Lekcije su uspješno dodane u nastavni plan!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Lekcije nisu uspješno dodane u nastavni plan!");
               }
           );
        };

        var getNastavniPlanById = function (nastavniPlanId) {
            nastavniPlanServices.getById(nastavniPlanId).then(
                function (response) {
                    $scope.nastavniPlan = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Nastavni plan nije učitan!");
                }
            );
        };

        $scope.getLekcijeByRazredByPlan = function (razred) {
            $scope.isLoading2 = true;
            services.getLekcijeByRazredByPlan(razred, nastavniPlanId).then(
                function (response) {
                    $scope.lekcije = response.data;
                    $scope.isLoading2 = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lekcije nisu učitane!");
                    $scope.isLoading2 = false;
                }
            );
        };

        //Other functions
        $scope.setSelected = function () {
            $scope.newForm.$setUntouched();
            $scope.selectedItems = [];
            $scope.getLekcijeByRazredByPlan($scope.selectedRazred);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selectedRazred = $scope.razredi[0];
            $scope.selectedItems = [];
            $scope.newForm.$setUntouched();
        };

        var addItem = function(item) {
            $scope.selectedItems.push(item);
        };

        var removeItem = function (item) {
            $scope.selectedItems.splice($scope.selectedItems.indexOf(item), 1);
        };

        $scope.manageItems = function (item) {
            if ($scope.selectedItems.indexOf(item) == -1)
                addItem(item);
            else
                removeItem(item);
        };


        getAll(nastavniPlanId);
        getNastavniPlanById(nastavniPlanId);
    });

}());

