(function () {

    mektebApp.controller("homeController", function ($scope, localStorageService, statisticsServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;

        var services = statisticsServices;

        var getAll = function () {
            $scope.isLoading = true;
            services.getCounters().then(
                function (response) {
                    $scope.data = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Statistike nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        getAll();
    });

}());

