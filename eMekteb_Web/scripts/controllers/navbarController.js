﻿(function () {

    mektebApp.controller("navbarController", function ($location, $scope, localStorageService, porukeServices) {
        
        $scope.logout = function () {
            localStorageService.remove('authorizationData');
            $location.path('/home');
        };

        //check if user logged in
        if (!localStorageService.get('authorizationData'))
            $scope.logout();
        else {

            var services = porukeServices;

            $scope.loggedUser = localStorageService.get('authorizationData').loggedUser.data;
            $scope.name = $scope.loggedUser.ime + ' ' + $scope.loggedUser.prezime;

            var getTop4Conversations = function (osobaId) {
                services.getAllConversations(osobaId).then(
                    function (response) {
                        $scope.conversations = response.data.slice(0, 4);
                    },
                    function (error) {
                        notificationsConfig.error("Desila se greška. Poruke nisu učitane!");
                    }
                );
            };

            $scope.openMessenger = function(id1, id2) {
                localStorageService.set('messengerData', { id1: id1, id2: id2 });
                $location.path('/poruke');
            };

            $scope.getClass = function (path) {
                return ($location.path().substr(0, path.length) === path) ? 'fa-circle' : 'fa-circle-thin';
            };

            getTop4Conversations($scope.loggedUser.id);
        }

    });

}());

