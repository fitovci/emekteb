﻿(function () {

    mektebApp.controller("lekcijeController", function ($scope, localStorageService, lekcijeServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        $scope.razredi = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        $scope.selectedRazred = $scope.razredi[0];

        var services = lekcijeServices;

        //CRUD functions
        $scope.getAll = function (razredId) {
            $scope.isLoading = true;
            services.getAll(razredId).then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lekcije nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lekcije nije učitane!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   $scope.getAll($scope.selectedRazred);
                   notificationsConfig.success("Nova lekcija je uspješno dodana!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Nova lekcija nije uspješno dodana!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   $scope.getAll($scope.selectedRazred);
                   notificationsConfig.success("Lekcija je uspješno uređena!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Lekcija nije uspješno uređena!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    $scope.getAll($scope.selectedRazred);
                    notificationsConfig.success("Lekcija je uspješno obrisana!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Lekcija nije uspješno obrisana!");
               }
           );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };


        $scope.getAll($scope.selectedRazred);
    });

}());

