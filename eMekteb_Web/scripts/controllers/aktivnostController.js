﻿(function () {

    mektebApp.controller("aktivnostController", function ($q, $scope, $filter, localStorageService, aktivnostServices, mektebskaGodinaServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = aktivnostServices;

        //CRUD functions
        var getAll = function () {
            $scope.isLoading = true;
            services.getByGodina(0, true).then(
                    function (response) {
                        $scope.allItems = response.data;
                        $scope.isLoading = false;
                    },
                    function (error) {
                        notificationsConfig.error("Desila se greška. Aktivnosti nisu učitani!");
                        $scope.isLoading = false;
                    }
                );    
        };

        var getById = function (id) {
            var deferred = $q.defer();
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                    $scope.selected.datum = $filter('date')($scope.selected.datum, 'dd. MMMM yyyy');
                    $scope.selected.skolskaGodinaId = $scope.selected.skolskaGodinas;
                    deferred.resolve(response.data);
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Aktivnost nije učitana!");
                    deferred.reject(error);
                }
            );
            return deferred.promise;
        };

        $scope.add = function (item) {
            item.skolskaGodinaId = item.skolskaGodinaId.id;
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Nova aktivnost je uspješno dodana!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Nova aktivnost nije uspješno dodana!");
               }
           );
        };

        $scope.update = function (id, item) {
            item.skolskaGodinaId = item.skolskaGodinaId.id;
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Aktivnost je uspješno uređena!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Aktivnost nije uspješno uređena!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Aktivnost je uspješno obrisana!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Aktivnost nije uspješno obrisana!");
               }
           );
        };

        var getGodine = function () {
            $scope.isLoading = true;
            mektebskaGodinaServices.getAll().then(
                function (response) {
                    $scope.godine = response.data;
                    if ($scope.godine) 
                        $scope.selectedGodina = $scope.godine[0];
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mektebske godine nisu učitani!");
                    $scope.isLoading = false;
                }
            );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id).then(function () {
                getGodine();
            });
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };

        $scope.getByGodina = function (godina) {
            $scope.isLoading = true;
            services.getByGodina(godina, false).then(
                    function (response) {
                        $scope.allItems = response.data;
                        $scope.isLoading = false;
                    },
                    function (error) {
                        notificationsConfig.error("Desila se greška. Aktivnosti nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        }


        getAll();
        getGodine();
    });

}());

