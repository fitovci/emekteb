﻿(function () {

    mektebApp.controller("nastavniPlanController", function ($scope, $location, localStorageService, nastavniPlanServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = nastavniPlanServices;

        //CRUD functions
        var getAll = function () {
            $scope.isLoading = true;
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Nastavni planovi nisu učitani!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Nastavni plan nije učitan!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Novi nastavni plan je uspješno dodan!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Novi nastavni plan nije uspješno dodan!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Nastavni plan je uspješno uređen!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Nastavni plan nije uspješno uređen!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Nastavni plan je uspješno obrisan!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Nastavni plan nije uspješno obrisan!");
               }
           );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };

        $scope.manageNastavniPlan = function (id) {
            $location.path('/nastavniplan/' + id);
        };

        getAll();
    });

}());

