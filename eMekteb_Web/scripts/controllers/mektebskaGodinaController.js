﻿(function () {

    mektebApp.controller("mektebskaGodinaController", function ($scope, $filter, localStorageService, mektebskaGodinaServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = mektebskaGodinaServices;

        //CRUD functions
        var getAll = function () {
            $scope.isLoading = true;
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mektebske godine nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                    $scope.selected.pocinje = $filter('date')($scope.selected.pocinje, 'dd. MMMM yyyy');
                    $scope.selected.zavrsava = $filter('date')($scope.selected.zavrsava, 'dd. MMMM yyyy');
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mektebska godina nije učitana!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Nova mektebska godina je uspješno dodana!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Nova mektebska godina nije uspješno dodana!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Mektebska godina je uspješno uređena!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Mektebska godina nije uspješno uređena!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Mektebska godina je uspješno obrisana!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Mektebska godina nije uspješno obrisana!");
               }
           );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };


        getAll();

        

    });

}());

