﻿(function () {

    mektebApp.controller("edukatorController", function ($q, $scope, $filter, localStorageService, edukatorServices, mektebServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = edukatorServices;

        //CRUD functions
        var getAll = function () {
            $scope.isLoading = true;
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Edukatori nisu učitani!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            var deferred = $q.defer();
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                    $scope.selected.datumRodjenja = $filter('date')($scope.selected.datumRodjenja, 'dd. MMMM yyyy');
                    $scope.selected.mektebId = $scope.selected.mekteb;
                    deferred.resolve(response.data);
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Edukator nije učitan!");
                    deferred.reject(error);
                }
            );
            return deferred.promise;
        }; 

        $scope.add = function (item) {
            item.mektebId = item.mektebId.id;
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Novi edukator je uspješno dodan!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Novi edukator nije uspješno dodan!");
               }
           );
        };

        $scope.update = function (id, item) {
            item.mektebId = item.mektebId.id;
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Edukator je uspješno uređen!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Edukator nije uspješno uređen!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Edukator je uspješno obrisan!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Edukator nije uspješno obrisan!");
               }
           );
        };

        //Get Mektebs for dropdown
        var getAllMektebs = function () {
            var deferred = $q.defer();

            mektebServices.getAll().then(
                function (response) {                     
                    deferred.resolve(response.data);
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mektebi nisu učitani!");
                    deferred.reject(error);
                }
            );
            return deferred.promise;
        };

        var setMektebDropdown = function (id) {
            var found = $filter('filter')($scope.mektebi, { id: id }, true);
            if (found.length) {
                $scope.selectedMekteb = JSON.stringify(found[0]);
            } else {
                $scope.selectedMekteb = 'Not found';
            }
        };


        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getAllMektebs()
                .then(function (mektebs) {
                    $scope.mektebi = mektebs;
                })
                .then(function () {
                    getById(id)
                        .then(function () {
                            setMektebDropdown($scope.selected.mektebId);
                        });
                });
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };

        $scope.newModal = function (){
            getAllMektebs()
                .then(function (mektebs) {
                    $scope.mektebi = mektebs;
                })
        }

        getAll();

    });

}());

