﻿(function () {

    mektebApp.controller("porukeController", function ($scope, $location, localStorageService, porukeServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = porukeServices;
        $scope.poruka = { sadrzaj: '' };

        $scope.loggedUser = localStorageService.get('authorizationData').loggedUser.data;
        $scope.messengerData = localStorageService.get('messengerData');

        //CRUD functions
        var getAllConversations = function (osobaId) {
            services.getAllConversations(osobaId).then(
                function (response) {
                    $scope.conversations = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Poruke nisu učitane!");
                }
            );
        };

        var getMessagesFromConversation = function (id1, id2) {
            services.getMessagesFromConversation(id1, id2).then(
                function (response) {
                    $scope.messages = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Poruke nisu učitane!");
                }
            );
        };

        $scope.sendMessage = function (sadrzaj) {
            var poruka =
            {
                sadrzaj: sadrzaj,
                datum: new Date(),
                procitana: false,
                primaocId: localStorageService.get('messengerData').id1 === $scope.loggedUser.id ? localStorageService.get('messengerData').id2 : localStorageService.get('messengerData').id1,
                posiljaocId: $scope.loggedUser.id
            };
            services.post(poruka).then(
               function () {
                   $scope.poruka.sadrzaj = "";
                   getAllConversations($scope.loggedUser.id);
                   getMessagesFromConversation(localStorageService.get('messengerData').id1, localStorageService.get('messengerData').id2);
               },
               function () {
                   notificationsConfig.error("Desila se greška. Nova poruka nije poslana uspješno!");
               }
           );
        };

        $scope.getModalData = function () {
            services.getOsobasForConversation().then(
                function (response) {
                    $scope.osobe = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Osobe nisu učitane!");
                }
            );
        };

        $scope.createNewChat = function(primaocId) {
            $scope.newMessage = true;
            $scope.messages = undefined;
            localStorageService.set('messengerData', { id1: localStorageService.get('authorizationData').loggedUser.data.id, id2: primaocId });
        };

        //Other functions
        $scope.openChat = function (id1, id2) {
            localStorageService.set('messengerData', { id1: id1, id2: id2 });
            getMessagesFromConversation(id1, id2);
        };

        getAllConversations($scope.loggedUser.id);
        if (localStorageService.get('messengerData').id1 && localStorageService.get('messengerData').id2) {
            getMessagesFromConversation(localStorageService.get('messengerData').id1, localStorageService.get('messengerData').id2);
        }

    });

}());

