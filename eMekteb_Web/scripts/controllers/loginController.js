﻿(function () {

    mektebApp.controller("loginController", function ($scope, $location, localStorageService, loginServices) {

        $scope.isLoading = false;
        var services = loginServices;

        $scope.login = function (email, pw) {

            $scope.isLoading = true;
            var item = { email: email, password: pw };

            services.post(item).then(
               function (data) {
                   $scope.isLoading = false;
                   localStorageService.set('authorizationData', { loggedUser: data });
                   $location.path('/dashboard');
               },
               function (error) {
                   $scope.isLoading = false;
                   $scope.resetForm();
                   notificationsConfig.error("Prijava na sistem je bila neuspješna. Pokušajte ponovo!");
               }
           );
        };

        $scope.resetForm = function () {
            $scope.formLogin.$setUntouched();
            $scope.formLogin.email.$dirty = false;
            $scope.formLogin.password.$dirty = false;
            $scope.email = $scope.password = null;
        };

    });

}());

