﻿(function () {

    mektebApp.controller("mektebController", function ($scope, localStorageService, mektebServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;
        var services = mektebServices;

        //CRUD functions
        var getAll = function () {
            $scope.isLoading = true;
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mektebi nisu učitani!");
                    $scope.isLoading = false;
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Mekteb nije učitan!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Novi mekteb je uspješno dodan!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Novi mekteb nije uspješno dodan!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Mekteb je uspješno uređen!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Mekteb nije uspješno uređen!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Mekteb je uspješno obrisan!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Mekteb nije uspješno obrisan!");
               }
           );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };


        getAll();
    });

}());

