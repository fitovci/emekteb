﻿(function () {

    mektebApp.controller("dashboardController", function ($scope, localStorageService, statisticsServices) {

        $scope.isEdit = false;
        $scope.isLoading = true;

        var services = statisticsServices;

        var getAll = function () {
            $scope.isLoading = true;
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Statistike nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        var getCounters = function () {
            $scope.isLoading = true;
            services.getCounters().then(
                function (response) {
                    $scope.brojUcenika = response.data.brojPolaznika;
                    $scope.brojMekteba = response.data.brojMekteba;
                    $scope.isLoading = false;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Statistike nisu učitane!");
                    $scope.isLoading = false;
                }
            );
        };

        $scope.gauge_options = {thickness: 7, mode: "gauge", total: 5};

        $scope.gauge_data = [
            { label: "Prosjek ocjena", value: 4.3, color: "steelblue" }
        ];

        getAll();
        getCounters();
    });

}());

