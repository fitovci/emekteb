﻿(function () {
    "use strict";

    ucenikApp.factory("mapServices", ['$http', 'Config', function ($http, Config) {

        var getLokacije = function () {
            return $http.get(Config.source + 'lokacijes/GetLokacije/');
        };

        return {
            getLokacije: getLokacije
        };

    }]);

})();