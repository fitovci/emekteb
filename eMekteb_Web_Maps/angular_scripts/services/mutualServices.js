﻿(function () {
    "use strict";

    ucenikApp.factory("mutualServices", ['$http', 'Config', function ($http, Config) {

        var getLokacije = function () {
            return $http.get(Config.source + 'vakufi');
        };
        
        var getLokacija = function (vakufId) {
            return $http.get(Config.source + 'vakufi/' + vakufId);
        };

        var postLokacija = function (vakuf) {
            return $http.post(Config.source + 'vakufi', vakuf);
        };

        var deleteLokacija = function (vakufId) {
            return $http.delete(Config.source + 'vakufi/' + vakufId);
        };
       
        var updateLokacija = function (vakufId, vakuf) {
            return $http.put(Config.source + 'vakufi/' + vakufId, vakuf);
        };

        return {
            getLokacije: getLokacije,
            getLokacija: getLokacija,
            postLokacija: postLokacija,
            deleteLokacija: deleteLokacija,
            updateLokacija: updateLokacija

        };

    }]);

})();