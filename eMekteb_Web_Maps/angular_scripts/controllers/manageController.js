﻿(function () {

    ucenikApp.controller("manageController", function ($q, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;

        $scope.loggedUser = localStorageService.get('authorizationMaps').loggedUser.data;

       
        //CRUD functions
        var getLokacije = function () {
            services.getLokacije().then(
                function (response) {
                    $scope.lokacije = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lokacije nisu učitane!");
                }
            );
        };

        var getById = function (id) {
            var deferred = $q.defer();
            services.getLokacija(id).then(
                function (response) {
                    $scope.selected = response.data;
                    deferred.resolve(response.data);
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lokacija nije učitana!");
                    deferred.reject(error);
                }
            );
            return deferred.promise;
        };

        $scope.update = function (id, item) {
            services.updateLokacija(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getLokacije();
                   notificationsConfig.success("Lokacija je uspješno uređena!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Lokacija nije uspješno uređena!");
               }
           );
        };

        $scope.remove = function (id) {
            services.deleteLokacija(id).then(
                function (response) {
                    getLokacije();
                    notificationsConfig.success("Lokacija je uspješno obrisana!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Lokacija nije uspješno obrisana!");
               }
           );
        };

        $scope.setSelected = function(id) {
            $scope.newForm.$setUntouched();
            getById(id);
        };

        $scope.resetForm = function () {
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };

        getLokacije();

    });

}());

