﻿(function () {

    ucenikApp.controller("sidebarController", function ($location, $scope, localStorageService) {

        $scope.logout = function () {
            localStorageService.remove('authorizationMaps');
            $location.path('/login');
        };

        //check if user logged in
        if (!localStorageService.get('authorizationMaps'))
            $scope.logout();
        else {

            $scope.loggedUser = localStorageService.get('authorizationMaps').loggedUser.data;
            $scope.name = $scope.loggedUser.ime + ' ' + $scope.loggedUser.prezime;

            $scope.getClass = function (path) {
                return ($location.path().substr(0, path.length) === path) ? 'active' : '';
            };
        }

    });

}());

