﻿(function () {

    ucenikApp.controller("adminController", function ($q, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;

        $scope.loggedUser = localStorageService.get('authorizationMaps').loggedUser.data;

        var disableForm = function (value) {
            $('#title').prop('disabled', value);
            $('#desc').prop('disabled', value);
            $('#url-input').prop('disabled', value);
            $('#btnSubmit').prop('disabled', value);
        };

        $scope.resetNewLocation = function () {
            $scope.newLocation = {
                naziv: '',
                opis: '',
                lat: '',
                lon: '',
                url: ''
            };
            if ($scope.lokacije) {
                $scope.lokacije.pop();
            }
            disableForm(true);
        };

        $scope.resetNewLocation();

        

        //CRUD functions
        var getLokacije = function () {
            services.getLokacije().then(
                function (response) {
                    $scope.lokacije = response.data;
                    for (var i = 0; i < $scope.lokacije.length; i++) {
                        $scope.lokacije[i].id = $scope.lokacije[i].vakufId;
                        $scope.lokacije[i].latitude = $scope.lokacije[i].lat;
                        $scope.lokacije[i].longitude = $scope.lokacije[i].lon;
                    }
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lokacije nisu učitane!");
                }
            );
        };

        $scope.postLokacija = function () {
            services.postLokacija($scope.newLocation).then(
                function () {
                    getLokacije();
                    $scope.resetNewLocation();
                    notificationsConfig.success("Novi vakuf je uspješno dodan!");
                },
                function () {
                    notificationsConfig.error("Desila se greška. Lokacije nisu učitane!");
                }
            );
        };

        $scope.options = { scrollwheel: false };

        $scope.map = {
            center: { latitude: 43.342611, longitude: 17.811713 }, //Mostar, do not touch!!
            zoom: 14,
            bounds: {
                northeast: {
                    latitude: 47.2022433,
                    longitude: 18.2453568
                },
                southwest: {
                    latitude: 40.2022433,
                    longitude: 16.2453568
                }
            },
            events: {
                click: function (map, eventName, originalEventArgs) {
                    if ($scope.newLocation.lon == '' && $scope.newLocation.lat == '') {
                        var e = originalEventArgs[0];
                        var lat = e.latLng.lat(), lon = e.latLng.lng();
                        var marker = {
                            id: Date.now(),
                            latitude: lat,
                            longitude: lon,
                            coords: {
                                latitude: lat,
                                longitude: lon
                            },
                            title: 'm' + Date.now(),
                            show: false
                        };

                        $scope.lokacije.push(marker);

                        $scope.newLocation.lat = marker.latitude;
                        $scope.newLocation.lon = marker.longitude;

                        disableForm(false);
                    }
                }
            }
        };

        $scope.onClick = function (marker, eventName, model) {
            model.show = !model.show;
        };


        getLokacije();
        disableForm(true);

    });

}());

