﻿(function () {

    ucenikApp.controller("mapController", function ($q, $rootScope, $scope, $filter, localStorageService, mutualServices) {

        var services = mutualServices;
        $scope.options = { scrollwheel: false };

        var getLokacije = function () {
            services.getLokacije().then(
                function (response) {
                    $scope.lokacije = response.data;
                    for (var i = 0; i < $scope.lokacije.length; i++) {
                        $scope.lokacije[i].id = $scope.lokacije[i].vakufId;
                        $scope.lokacije[i].latitude = $scope.lokacije[i].lat;
                        $scope.lokacije[i].longitude = $scope.lokacije[i].lon;
                    }
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Lokacije nisu učitane!");
                }
            );
        };

        $scope.map = {
            center: { latitude: 43.342611, longitude: 17.811713 }, //Mostar, do not touch!!
            zoom: 14,
            bounds: {
                northeast: {
                    latitude: 47.2022433,
                    longitude: 18.2453568
                },
                southwest: {
                    latitude: 40.2022433,
                    longitude: 16.2453568
                }
            }
        };

        getLokacije();

    });

}());

