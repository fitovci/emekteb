﻿
ucenikApp.controller('confirmModalController', function ($scope, $uibModalInstance, message) {

    $scope.message = message;

    $scope.cancel = function () {
        $uibModalInstance.close(false);
    }

    $scope.continue = function () {
        $uibModalInstance.close(true);
    }
});