﻿
var ucenikApp = angular.module("mapsApp", [
    'ngRoute',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngMessages',
    'ngAnimate',
    'uiGmapgoogle-maps'
]);

ucenikApp.constant("Config", {
    source: "http://localhost:5757/api/"
});

ucenikApp.run(function ($rootScope, $location, localStorageService) {
    var loggedUser = localStorageService.get('authorizationMaps');
    if (!loggedUser || loggedUser === null || loggedUser === undefined) {
        $location.path('/login');
    }

    $rootScope.activeURL = $location;
});

ucenikApp.config(function ($routeProvider) {
    $routeProvider
        .when("/mapa", {
            templateUrl: "angular_views/map.html",
            controller: "mapController"
        })
        .when("/admin", {
            templateUrl: "angular_views/admin.html",
            controller: "adminController"
        })
        .when("/login", {
            templateUrl: "angular_views/login.html",
            controller: "loginController"
        })
        .when("/manage", {
            templateUrl: "angular_views/manage.html",
            controller: "manageController"
        })
        .otherwise({ redirectTo: "/mapa" });
});
