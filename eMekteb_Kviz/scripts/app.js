﻿
var kvizApp = angular.module("kvizApp", [
    'ngRoute',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngMessages', 
    'ngAnimate',
    'angularSpinner'
]);

kvizApp.constant("Config", {
    source: "http://localhost:5757/api/"
});

kvizApp.run(function ($location) {
   
});

kvizApp.config(function ($routeProvider) {
    $routeProvider
        .when("/login", {
            templateUrl: "views/loginView.html",
            controller: "loginController"
        })
        .when("/edukatori", {
            templateUrl: "views/edukatorView.html",
            controller: "edukatorController"
        })
        .otherwise({ redirectTo: "/home" });
});
