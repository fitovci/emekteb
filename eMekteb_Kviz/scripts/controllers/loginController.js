﻿(function () {

    kvizApp.controller("loginController", function ($scope, localStorageService, usSpinnerService) {
        
        var spinneractive = false;

        var startSpin = function () {
            if (!$scope.spinneractive) {
                usSpinnerService.spin('spinner-1');
                $scope.spinneractive = true;
            }
        };

        var stopSpin = function () {
            if ($scope.spinneractive) {
                usSpinnerService.stop('spinner-1');
                $scope.spinneractive = false;
            }
        };

    });

}());

