﻿(function () {

    kvizApp.controller("edukatorController", function ($scope, localStorageService, usSpinnerService, edukatorServices) {

        $scope.isEdit = false;
        var spinnerActive = false;
        var services = edukatorServices;

        //CRUD functions
        var getAll = function () {
            services.getAll().then(
                function (response) {
                    $scope.allItems = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Edukatori nisu učitani!");
                }
            );
        };

        var getById = function (id) {
            services.getById(id).then(
                function (response) {
                    $scope.selected = response.data;
                },
                function (error) {
                    notificationsConfig.error("Desila se greška. Edukator nije učitan!");
                }
            );
        };

        $scope.add = function (item) {
            services.post(item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Novi edukator je uspješno dodan!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Novi edukator nije uspješno dodan!");
               }
           );
        };

        $scope.update = function (id, item) {
            services.put(id, item).then(
               function (response) {
                   $scope.resetForm();
                   getAll();
                   notificationsConfig.success("Edukator je uspješno uređen!");
               },
               function (error) {
                   $scope.resetForm();
                   notificationsConfig.error("Desila se greška. Edukator nije uspješno uređen!");
               }
           );
        };

        $scope.remove = function (id) {
            services.remove(id).then(
                function (response) {
                    getAll();
                    notificationsConfig.success("Edukator je uspješno obrisan!");
                },
               function (error) {
                   notificationsConfig.error("Desila se greška. Edukator nije uspješno obrisan!");
               }
           );
        };

        //Other functions
        $scope.setSelected = function (id) {
            $scope.newForm.$setUntouched();
            getById(id);
            $scope.isEdit = true;
        }

        $scope.resetForm = function () {
            $scope.isEdit = false;
            $scope.selected = null;
            $scope.newForm.$setUntouched();
        };

        //Spinner functions
        $scope.startSpin = function () {
            if (!$scope.spinneractive) {
                usSpinnerService.spin('spinner-1');
                $scope.spinneractive = true;
            }
        };

        var stopSpin = function () {
            if ($scope.spinneractive) {
                usSpinnerService.stop('spinner-1');
                $scope.spinneractive = false;
            }
        };


        getAll();

    });

}());

