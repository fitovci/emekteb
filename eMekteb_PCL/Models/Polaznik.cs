//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PeP_PCL.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Polaznik
    {
        public Polaznik()
        {
            //this.EvidencijaNapredovanjas = new HashSet<EvidencijaNapredovanjas>();
            //this.Zadacas = new HashSet<Zadacas>();
            //this.Nastavas = new HashSet<Nastavas>();
        }
    
        public int Id { get; set; }
        public int RazredId { get; set; }
        public int UcenikId { get; set; }
        public string FinalnaOcjena { get; set; }
        public string Komentar { get; set; }
        public string Vladanje { get; set; }
        public Nullable<int> StepenId { get; set; }
        public string SkolskiRazred { get; set; }
    /*
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EvidencijaNapredovanjas> EvidencijaNapredovanjas { get; set; }
        public virtual Razreds Razreds { get; set; }
        public virtual Stepens Stepens { get; set; }
        public virtual Uceniks Uceniks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Zadacas> Zadacas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nastavas> Nastavas { get; set; }
    */
    }
}
