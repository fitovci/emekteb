﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class ClanciVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Autor { get; set; }
        public string Datum { get; set; }
        public string Sadrzaj { get; set; }
    }
}
