﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class PorukePost
    {
        public int Id { get; set; }
        public string Naslov { get; set; }
        public string Sadrzaj { get; set; }
        public System.DateTime Datum { get; set; }
        public bool Procitana { get; set; }
        public int PrimaocId { get; set; }
        public int PosiljaocId { get; set; }
    }
}
