﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class UcenikVM
    {
        public int PolaznikId { get; set; }
        public int UcenikId { get; set; }
        public int OsobaId { get; set; }
        public string Napomena { get; set; }
        public string ImeRoditelja { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Kontakt { get; set; }
        public string Adresa { get; set; }
        public string Email { get; set; }
        public System.DateTime DatumRodjenja { get; set; }
        public string KorisnickoIme { get; set; }
        public string LozinkaHash { get; set; }
        public string LozinkaSalt { get; set; }
        public string ImageURL { get; set; }
        public string Opis { get; set; }

        public string Mekteb { get; set; }
        public string Razred { get; set; }

        public int MaxPrisustvo { get; set; }
        public int Prisustvo { get; set; }
        public int MaxNapredovanje { get; set; }
        public int Napredovanje { get; set; }
        public decimal ProsjekOcjena { get; set; }
    }
}
