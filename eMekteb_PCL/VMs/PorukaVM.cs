﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_PCL.VMs
{
    public class PorukaVM
    {
        public string ImePrezime { get; set; }
        public string Poruka { get; set; }
        public DateTime Datum { get; set; }
        public int? PrimaocId { get; set; }
        public int? PosiljaocId { get; set; }
    }
}