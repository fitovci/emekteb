﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class ZadacaVM
    {
        public int Id { get; set; }
        public string Lekcija { get; set; }
        public string Datum { get; set; }
        public bool Zavrsena { get; set; }

    }
}
