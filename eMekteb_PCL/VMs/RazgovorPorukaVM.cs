﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_PCL.VMs
{
    public class RazgovorPorukaVM
    {
        public string Poruka { get; set; }
        public string Primaoc { get; set; }
        public string Posiljaoc { get; set; }
        public DateTime Datum { get; set; }
        public int? posiljaocId { get; set; }
        public int? primaocId { get; set; }
    }
}