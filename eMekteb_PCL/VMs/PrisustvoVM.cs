﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class PrisustvoVM
    {
        public int Id { get; set; }
        public int RedniBroj { get; set; }
        public string Lekcija { get; set; }
        public string Datum { get; set; }
    }
}
