﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeP_PCL.VMs
{
    public class SlikaVM
    {
        public byte[] Slika { get; set; }
        public byte[] SlikaThumb { get; set; }
    }
}
