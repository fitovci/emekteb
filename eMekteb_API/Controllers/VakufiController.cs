﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;

namespace PeP_API.Controllers
{
    public class VakufiController : ApiController
    {
        private db_entities db = new db_entities();

        // GET: api/Vakufi
        public IQueryable<Vakufi> GetVakufi()
        {
            return db.Vakufi;
        }

        // GET: api/Vakufi/5
        [ResponseType(typeof(Vakufi))]
        public IHttpActionResult GetVakufi(int id)
        {
            Vakufi vakufi = db.Vakufi.Find(id);
            if (vakufi == null)
            {
                return NotFound();
            }

            return Ok(vakufi);
        }

        // PUT: api/Vakufi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVakufi(int id, Vakufi vakufi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vakufi.VakufId)
            {
                return BadRequest();
            }

            db.Entry(vakufi).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VakufiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vakufi
        [ResponseType(typeof(Vakufi))]
        public IHttpActionResult PostVakufi(Vakufi vakufi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vakufi.Add(vakufi);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vakufi.VakufId }, vakufi);
        }

        // DELETE: api/Vakufi/5
        [ResponseType(typeof(Vakufi))]
        public IHttpActionResult DeleteVakufi(int id)
        {
            Vakufi vakufi = db.Vakufi.Find(id);
            if (vakufi == null)
            {
                return NotFound();
            }

            db.Vakufi.Remove(vakufi);
            db.SaveChanges();

            return Ok(vakufi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VakufiExists(int id)
        {
            return db.Vakufi.Count(e => e.VakufId == id) > 0;
        }
    }
}