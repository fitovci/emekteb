﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;
using PeP_PCL.VMs;

namespace PeP_API.Controllers
{
    public class PorukesController : ApiController
    {
        db_entities _db = new db_entities();

        [HttpGet]
        [Route("api/Porukes/GetAllConversations/{id}")]
        public List<PorukaVM> GetAllConversations(int id)
        {
            var podaci = from um in _db.Porukes
                         where um.PosiljaocId == id || um.PrimaocId == id
                         let otherId = (um.PosiljaocId == id) ? um.PrimaocId : um.PosiljaocId
                         group um by otherId
                             into g
                         select g.OrderByDescending(um => um.Datum).FirstOrDefault();

            List<PorukaVM> lista = podaci.Where(x => x.PosiljaocId == id).Select(y => new PorukaVM
            {
                ImePrezime = y.Osobas.Ime + " " + y.Osobas.Prezime,
                Datum = y.Datum,
                Poruka = y.Sadrzaj.Substring(0, 40),
                PosiljaocId = y.PosiljaocId,
                PrimaocId = y.PrimaocId
            }).OrderByDescending(l => l.Datum).ToList();

            List<PorukaVM> lista2 = podaci.Where(x => x.PrimaocId == id).Select(y => new PorukaVM
            {
                ImePrezime = y.Osobas1.Ime + " " + y.Osobas1.Prezime,
                Datum = y.Datum,
                Poruka = y.Sadrzaj.Substring(0, 40),
                PosiljaocId = y.PosiljaocId,
                PrimaocId = y.PrimaocId
            }).OrderByDescending(l => l.Datum).ToList();

            lista.AddRange(lista2);

            return lista.OrderByDescending(l => l.Datum).ToList();
        }

        [HttpGet]
        [Route("api/porukes/GetMessagesFromConversation/{id1}/{id2}")]
        public List<RazgovorPorukaVM> GetMessagesFromConversation(int id1, int id2)
        {
            List<RazgovorPorukaVM> lista = _db.Porukes
                                       .Where(x => x.PosiljaocId == id1 && x.PrimaocId == id2)
                                       .Select(x => new RazgovorPorukaVM
                                       {
                                           Poruka = x.Sadrzaj,
                                           Posiljaoc = x.Osobas1.Ime + " " + x.Osobas1.Prezime,
                                           Primaoc = x.Osobas.Ime + " " + x.Osobas.Prezime,
                                           Datum = x.Datum,
                                           posiljaocId = x.PosiljaocId,
                                           primaocId = x.PrimaocId
                                       }).ToList();

            List<RazgovorPorukaVM> lista2 = _db.Porukes
                                       .Where(x => x.PosiljaocId == id2 && x.PrimaocId == id1)
                                       .Select(x => new RazgovorPorukaVM
                                       {
                                           Poruka = x.Sadrzaj,
                                           Posiljaoc = x.Osobas1.Ime + " " + x.Osobas1.Prezime,
                                           Primaoc = x.Osobas.Ime + " " + x.Osobas.Prezime,
                                           Datum = x.Datum,
                                           posiljaocId = x.PosiljaocId,
                                           primaocId = x.PrimaocId
                                       }).ToList();

            lista.AddRange(lista2);

            return lista.OrderByDescending(x => x.Datum).ToList();
        }

        // GET: api/Porukes
        public IQueryable<Porukes> GetPorukes()
        {
            return _db.Porukes;
        }

        // GET: api/Porukes/5
        [ResponseType(typeof(Porukes))]
        public IHttpActionResult GetPorukes(int id)
        {
            Porukes porukes = _db.Porukes.Find(id);
            if (porukes == null)
            {
                return NotFound();
            }

            return Ok(porukes);
        }

        // PUT: api/Porukes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPorukes(int id, Porukes porukes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != porukes.Id)
            {
                return BadRequest();
            }

            _db.Entry(porukes).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PorukesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Porukes
        [ResponseType(typeof(Porukes))]
        public IHttpActionResult PostPorukes(Porukes porukes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Porukes.Add(porukes);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = porukes.Id }, porukes);
        }

        // DELETE: api/Porukes/5
        [ResponseType(typeof(Porukes))]
        public IHttpActionResult DeletePorukes(int id)
        {
            Porukes porukes = _db.Porukes.Find(id);
            if (porukes == null)
            {
                return NotFound();
            }

            _db.Porukes.Remove(porukes);
            _db.SaveChanges();

            return Ok(porukes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PorukesExists(int id)
        {
            return _db.Porukes.Count(e => e.Id == id) > 0;
        }
    }
}