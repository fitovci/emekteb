﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;
using PeP_PCL.VMs;

namespace PeP_API.Controllers
{
    public class PolazniksController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/Polazniks
        public IHttpActionResult GetPolaznik()
        {
            try
            {
                var polaznici = _db.Polaznik;
                if (polaznici == null)
                    return NotFound();

                return Ok(polaznici);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/Polazniks/GetIzostanci/{ucenikId}")]
        public IHttpActionResult GetIzostanci(int ucenikId)
        {
            try
            {
                int razredId = _db.Polaznik.Where(x => x.UcenikId == ucenikId).Select(x => x.RazredId).FirstOrDefault();

                Polaznik p = _db.Polaznik.Where(x => x.UcenikId == ucenikId).FirstOrDefault();
                if (p == null)
                    return NotFound();

                List<Nastavas> sviCasovList =
                    _db.Nastavas.Where(x => x.RazredId == razredId).Include(x => x.Polaznik).Include(x => x.Lekcijas).ToList();

                List<Nastavas> nesto = sviCasovList.Where(x => !x.Polaznik.Contains(p)).OrderByDescending(x => x.Datum).ToList();

                List<PrisustvoVM> prisustvo = nesto.Select(x => new PrisustvoVM
                {
                    Id = x.Id,
                    Datum = x.Datum.ToString(),
                    Lekcija = x.Lekcijas.Naziv
                }).ToList();

                DateTime temp;
                for (int i = 0; i < prisustvo.Count; i++)
                {
                    prisustvo[i].RedniBroj = prisustvo.Count - i;
                    temp = Convert.ToDateTime(prisustvo[i].Datum);
                    prisustvo[i].Datum = temp.ToString("D");
                }

                return Ok(prisustvo);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Polazniks/5
        [ResponseType(typeof(Polaznik))]
        public IHttpActionResult GetPolaznik(int id)
        {
            try
            {
                Polaznik polaznik = _db.Polaznik.Find(id);
                if (polaznik == null)
                {
                    return NotFound();
                }

                return Ok(polaznik);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [ResponseType(typeof(List<Nastavas>))]
        [Route("api/Polazniks/GetPrisustvoByPolaznikId/{polaznikId}")]
        public IHttpActionResult GetPrisustvoByPolaznikId(int polaznikId)
        {
            try
            {
                var p = _db.Nastavas.Where(x => x.Polaznik.Select(y=>y.Id).Contains(polaznikId)).Include(x=>x.Lekcijas).OrderByDescending(x=>x.Datum).ToList();
                if (p == null)
                {
                    return NotFound();
                }

                return Ok(p);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Polazniks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPolaznik(int id, Polaznik polaznik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != polaznik.Id)
            {
                return BadRequest();
            }

            _db.Entry(polaznik).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PolaznikExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Polazniks
        [ResponseType(typeof(Polaznik))]
        public IHttpActionResult PostPolaznik(Polaznik polaznik)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Polaznik.Add(polaznik);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = polaznik.Id }, polaznik);
        }

        // DELETE: api/Polazniks/5
        [ResponseType(typeof(Polaznik))]
        public IHttpActionResult DeletePolaznik(int id)
        {
            Polaznik polaznik = _db.Polaznik.Find(id);
            if (polaznik == null)
            {
                return NotFound();
            }

            _db.Polaznik.Remove(polaznik);
            _db.SaveChanges();

            return Ok(polaznik);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PolaznikExists(int id)
        {
            return _db.Polaznik.Count(e => e.Id == id) > 0;
        }
    }
}