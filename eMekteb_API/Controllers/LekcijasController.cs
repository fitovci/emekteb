﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs;
using PeP_API.Models;

namespace PeP_API.Controllers
{
    public class LekcijasController : ApiController
    {
        db_entities _db = new db_entities();

        [HttpGet]
        [Route("api/Lekcijas/GetByRazred/{razred}")]
        public IHttpActionResult GetByRazred(int razred = 0)
        {
            try
            {
                List<Lekcijas> lekcije = _db.Lekcijas.Where(x => x.Razred == razred).ToList();

                return Ok(lekcije);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Lekcijas
        [HttpGet]
        [Route("api/Lekcijas/GetLekcije/{razred}/{planId}")]
        public List<LekcijaVM> GetLekcije(int razred = 0, int planId = 0)
        {
            List<LekcijaVM> Lista;
            NastavniPlans Plan = _db.NastavniPlans.Where(x => x.Id == planId).Include(x => x.Lekcijas).FirstOrDefault();

            List<LekcijaVM> Lekcije = Plan.Lekcijas.Select(x => new LekcijaVM
            {
                Id = x.Id,
                Naziv = x.Naziv
            }).ToList();

            if (razred == 0)
            {
                Lista = _db.Lekcijas.Select(x => new LekcijaVM
                {
                    Id = x.Id,
                    Naziv = x.Naziv
                }).ToList();
            }
            else
                Lista = _db.Lekcijas.Where(x => x.Razred == razred).Select(x => new LekcijaVM
                {
                    Id = x.Id,
                    Naziv = x.Naziv
                }).ToList();

            for (int i = 0; i < Lista.Count; i++)
            {
                for (int y = 0; y < Lekcije.Count; y++)
                {
                    if (Lista[i].Id == Lekcije[y].Id)
                    {
                        Lista.Remove(Lista[i]);
                    }
                }
            }

            return Lista.ToList();
        }

        [HttpGet]
        [Route("api/Lekcijas/GetLekcijeByPlan/{planId}")]
        [ResponseType(typeof(List<LekcijaVM>))]
        public IHttpActionResult GetLekcijeByPlan(int planId)
        {
            try
            {
                NastavniPlans plan = _db.NastavniPlans.Where(x => x.Id == planId).Include(x => x.Lekcijas).FirstOrDefault();

                List<LekcijaVM> lekcije = plan.Lekcijas.Select(x => new LekcijaVM
                {
                    Id = x.Id,
                    Naziv = x.Naziv
                }).ToList();

                return Ok(lekcije);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<RazredVM>))]
        [Route("api/Lekcijas/GetRazreds")]
        public IHttpActionResult GetRazreds()
        {
            try
            {
                List<RazredVM> razredi = _db.Lekcijas.GroupBy(x => x.Razred).Select(x => new RazredVM
                {
                    Id = x.FirstOrDefault().Razred,
                    Naziv = x.FirstOrDefault().Razred.ToString()
                }).ToList();

                return Ok(razredi);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            
        }

        // GET: api/Lekcijas/5
        [ResponseType(typeof(Lekcijas))]
        public IHttpActionResult GetLekcijas(int id)
        {
            try
            {
                Lekcijas lekcijas = _db.Lekcijas.Find(id);
                if (lekcijas == null)
                {
                    return NotFound();
                }

                return Ok(lekcijas);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Lekcijas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLekcijas(int id, Lekcijas lekcijas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lekcijas.Id)
            {
                return BadRequest();
            }

            _db.Entry(lekcijas).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LekcijasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Lekcijas
        [ResponseType(typeof(Lekcijas))]
        public IHttpActionResult PostLekcijas(Lekcijas lekcijas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int redniBroj = _db.Lekcijas.Where(x => x.Razred == lekcijas.Razred).OrderByDescending(x => x.RedniBroj).Select(x => x.RedniBroj).FirstOrDefault();
            if (redniBroj > 0)
                lekcijas.RedniBroj = redniBroj + 1;
            else
                lekcijas.RedniBroj = 1;

            _db.Lekcijas.Add(lekcijas);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = lekcijas.Id }, lekcijas);
        }

        // DELETE: api/Lekcijas/5
        [ResponseType(typeof(Lekcijas))]
        public IHttpActionResult DeleteLekcijas(int id)
        {
            Lekcijas lekcijas = _db.Lekcijas.Find(id);
            if (lekcijas == null)
            {
                return NotFound();
            }

            _db.Lekcijas.Remove(lekcijas);
            _db.SaveChanges();

            return Ok(lekcijas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LekcijasExists(int id)
        {
            return _db.Lekcijas.Count(e => e.Id == id) > 0;
        }

        [HttpPost]
        [Route("api/Lekcijas/AddLekcijaToPlan/{planId}")]
        public void AddLekcijaToPlan(int planId, List<LekcijaVM> lista)
        {
            foreach (LekcijaVM x in lista)
            {
                _db.usp_AddLekcijaToPlan(x.Id, planId);
            }
        }

        [HttpPost]
        [Route("api/Lekcijas/DeleteLekcijaFromPlan/{planId}")]
        public void DeleteLekcijaFromPlan(int planId, List<LekcijaVM> lista)
        {
            foreach (LekcijaVM x in lista)
            {
                _db.usp_DeleteLekcijaFromPlan(x.Id, planId);
            }
        }
    }
}