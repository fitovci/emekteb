﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs.ReportsVMs;
using PeP_API.Models;
using System;

namespace PeP_API.Controllers
{
    public class MektebsController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/Mektebs
        public IHttpActionResult GetMektebs()
        {
            try
            {
                var mektebs =_db.Mektebs
                            .Include(x=>x.SluzbenaZabiljeskas)
                            .Include(x=>x.RoditeljskiSastanaks)
                            .Include(x=>x.Razreds)
                            .Where(x =>!x.IsDeleted);

                return Ok(mektebs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Mektebs/5
        [HttpGet]
        [Route("api/Mektebs/SearchMektebs/{naziv?}")]
        [ResponseType(typeof(usp_SearchMektebs_Result))]
        public IHttpActionResult SearchMektebs(string naziv = "")
        {
            try
            {
                List<usp_SearchMektebs_Result> mektebs = _db.usp_SearchMektebs(naziv).ToList();

                return Ok(mektebs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Mektebs/5
        [ResponseType(typeof(Mektebs))]
        public IHttpActionResult GetMektebs(int id)
        {
            try
            {
                Mektebs mektebs = _db.Mektebs.Find(id);
                if (mektebs == null)
                {
                    return NotFound();
                }

                return Ok(mektebs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Mektebs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMektebs(int id, Mektebs mektebs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mektebs.Id)
            {
                return BadRequest();
            }

            _db.Entry(mektebs).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MektebsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Mektebs
        [ResponseType(typeof(Mektebs))]
        public IHttpActionResult PostMektebs(Mektebs m)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            m.Id = _db.Mektebs.OrderByDescending(x => x.Id).FirstOrDefault().Id;
            m.MedzlisId = _db.Medzlis.OrderByDescending(x => x.Id).FirstOrDefault().Id;

            try
            {
                _db.usp_PostMekteb(m.Id + 1, m.MedzlisId, m.Naziv, m.Lokacija, false, 0);
            }
            catch (DbUpdateException)
            {
                if (MektebsExists(m.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = m.Id }, m);
        }

        // DELETE: api/Mektebs/5
        [Route("api/Mektebs/delete/{id}")]
        [ResponseType(typeof(Mektebs))]
        public IHttpActionResult DeleteMektebs(int id)
        {
            Mektebs mektebs = _db.Mektebs.Find(id);
            if (mektebs == null)
            {
                return NotFound();
            }

            _db.Mektebs.Remove(mektebs);
            _db.SaveChanges();

            return Ok(mektebs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MektebsExists(int id)
        {
            return _db.Mektebs.Count(e => e.Id == id) > 0;
        }

        [HttpGet]
        [Route("api/Mektebs/GetNumberOfPolazniksByMekteb")]
        public List<rptStatistikaVM> GetNumberOfPolazniksByMekteb()
        {
            Helpers.SkolskaGodinaManager manager = new Helpers.SkolskaGodinaManager();
            int TrenutnaGodinaId = manager.GetCurrentSkolskaGodinaId();

            List<Mektebs> Mektebi = _db.Mektebs.ToList();

            List<Razreds> Razredi;
            List<rptStatistikaVM> Statistika = new List<rptStatistikaVM>();

            foreach (Mektebs mekteb in Mektebi)
            {
                int ucenici = 0;

                Razredi = _db.Razreds.Where(x => x.MektebId == mekteb.Id && x.SkolskaGodinaId == TrenutnaGodinaId).Include(x => x.Polaznik).ToList();
                foreach (Razreds razred in Razredi)
                {
                    ucenici += razred.Polaznik.Count;
                }

                Statistika.Add(new rptStatistikaVM() { Mekteb = mekteb.Naziv, UpisanoDjece = ucenici });
            }

            return Statistika.OrderByDescending(x => x.UpisanoDjece).ToList();
        }


    }
}