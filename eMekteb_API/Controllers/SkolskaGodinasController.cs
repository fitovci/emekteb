﻿using System;
using PeP_API.Models;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace PeP_API.Controllers
{
    public class SkolskaGodinasController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/SkolskaGodinas
        public IQueryable<SkolskaGodinas> GetSkolskaGodinas()
        {
            return _db.SkolskaGodinas.OrderByDescending(x => x.Zavrsava);
        }

        // GET: api/SkolskaGodinas/5
        [ResponseType(typeof(SkolskaGodinas))]
        public IHttpActionResult GetSkolskaGodinas(int id)
        {
            try
            {
                SkolskaGodinas skolskaGodinas = _db.SkolskaGodinas
                                                .Include("kalendarskaAktivnosts")
                                                .Include("razreds")
                                                .Include("roditeljskiSastanaks")
                                                .Include("sluzbenaZabiljeskas")
                                                .FirstOrDefault(x => x.Id == id);
                if (skolskaGodinas == null)
                {
                    return NotFound();
                }

                return Ok(skolskaGodinas);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/SkolskaGodinas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSkolskaGodinas(int id, SkolskaGodinas skolskaGodinas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != skolskaGodinas.Id)
            {
                return BadRequest();
            }

            _db.Entry(skolskaGodinas).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SkolskaGodinasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SkolskaGodinas
        [ResponseType(typeof(SkolskaGodinas))]
        public IHttpActionResult PostSkolskaGodinas(SkolskaGodinas skolskaGodinas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.SkolskaGodinas.Add(skolskaGodinas);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = skolskaGodinas.Id }, skolskaGodinas);
        }

        // DELETE: api/SkolskaGodinas/5
        [HttpGet]
        [ResponseType(typeof(SkolskaGodinas))]
        [Route("api/SkolskaGodinas/delete/{id}")]
        public IHttpActionResult DeleteSkolskaGodinas(int id)
        {
            SkolskaGodinas skolskaGodinas = _db.SkolskaGodinas.Find(id);
            if (skolskaGodinas == null)
            {
                return NotFound();
            }

            _db.SkolskaGodinas.Remove(skolskaGodinas);
            _db.SaveChanges();

            return Ok(skolskaGodinas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SkolskaGodinasExists(int id)
        {
            return _db.SkolskaGodinas.Count(e => e.Id == id) > 0;
        }
    }
}