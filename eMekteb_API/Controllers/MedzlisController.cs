﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace PeP_API.Controllers
{
    public class MedzlisController : ApiController
    {

        //// GET: api/Medzlis
        //public IQueryable<Medzlis> GetMedzlis()
        //{
        //    return db.Medzlis;
        //}

        //// GET: api/Medzlis/5
        //[ResponseType(typeof(Medzlis))]
        //public IHttpActionResult GetMedzlis(int id)
        //{
        //    Medzlis medzlis = db.Medzlis.Find(id);
        //    if (medzlis == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(medzlis);
        //}

        //// PUT: api/Medzlis/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutMedzlis(int id, Medzlis medzlis)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != medzlis.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(medzlis).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!MedzlisExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Medzlis
        //[ResponseType(typeof(Medzlis))]
        //public IHttpActionResult PostMedzlis(Medzlis medzlis)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Medzlis.Add(medzlis);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = medzlis.Id }, medzlis);
        //}

        //// DELETE: api/Medzlis/5
        //[ResponseType(typeof(Medzlis))]
        //public IHttpActionResult DeleteMedzlis(int id)
        //{
        //    Medzlis medzlis = db.Medzlis.Find(id);
        //    if (medzlis == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Medzlis.Remove(medzlis);
        //    db.SaveChanges();

        //    return Ok(medzlis);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool MedzlisExists(int id)
        //{
        //    return db.Medzlis.Count(e => e.Id == id) > 0;
        //}
    }
}