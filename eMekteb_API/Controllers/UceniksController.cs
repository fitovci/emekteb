﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_PCL.VMs;
using PeP_API.Models;

namespace PeP_API.Controllers
{
    public class UceniksController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/Uceniks
        public IHttpActionResult GetUceniks()
        {
            try
            {
                var uceniks = _db.Uceniks;
                if (uceniks == null)
                    return NotFound();

                return Ok(uceniks);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Uceniks/5
        [ResponseType(typeof(Uceniks))]
        public IHttpActionResult GetUceniks(int id)
        {
            try
            {
                Uceniks uceniks = _db.Uceniks.Find(id);
                if (uceniks == null)
                    return NotFound();

                return Ok(uceniks);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/Uceniks/GetUcenikByUsername/{username}")]
        public IHttpActionResult GetUcenikByUsername(string username)
        {
            try
            {
                UcenikVM u = _db.Uceniks.Where(x => x.Osobas.KorisnickoIme == username).Select(x => new UcenikVM
                {
                    KorisnickoIme = x.Osobas.KorisnickoIme,
                    OsobaId = x.Osobas.Id,
                    Prezime = x.Osobas.Prezime,
                    Ime = x.Osobas.Ime,
                    LozinkaSalt = x.Osobas.LozinkaSalt,
                    LozinkaHash = x.Osobas.LozinkaHash,
                    Adresa = x.Osobas.Adresa,
                    DatumRodjenja = x.Osobas.DatumRodjenja,
                    Email = x.Osobas.Email,
                    ImeRoditelja = x.ImeRoditelja,
                    Kontakt = x.Osobas.Kontakt,
                    Napomena = x.Napomena,
                    UcenikId = x.Id,
                    ImageURL = x.Osobas.ImageURL, 
                    Opis = x.Osobas.Opis
                }).FirstOrDefault();

                if (u == null)
                    return NotFound();

                u.PolaznikId = _db.Polaznik.Where(x => x.UcenikId == u.UcenikId).OrderByDescending(x => x.Id).FirstOrDefault().Id;

                u.Mekteb =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Include(x => x.Razreds.Mektebs)
                        .Select(x => x.Razreds.Mektebs.Naziv)
                        .FirstOrDefault();

                u.Razred =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Include(x => x.Razreds)
                        .Select(x => x.Razreds.Naziv)
                        .FirstOrDefault();

                u.MaxPrisustvo =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Include(x => x.Razreds)
                        .Select(x => x.Razreds.Nastavas.Count)
                        .FirstOrDefault();

                u.Prisustvo =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Sum(x => x.Nastavas.Count);

                u.MaxNapredovanje =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Include(x => x.Razreds)
                        .Sum(x => x.Razreds.NastavniPlans.Lekcijas.Count);

                u.Napredovanje =
                    _db.Polaznik.Where(x => x.UcenikId == u.UcenikId)
                        .Sum(x => x.EvidencijaNapredovanjas.Count);

                var polaznikHistory =
                    _db.Polaznik.Include(x => x.EvidencijaNapredovanjas)
                        .Where(x => x.UcenikId == u.UcenikId);

                var sumOcjene = 0;
                var brojOcjena = 0;
                foreach (var polaznik in polaznikHistory)
                {
                    sumOcjene += polaznik.EvidencijaNapredovanjas.Sum(x => x.Ocjena);
                    brojOcjena += polaznik.EvidencijaNapredovanjas.Count;
                }

                u.ProsjekOcjena = decimal.Round((decimal)sumOcjene / brojOcjena, 2);


                return Ok(u);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Uceniks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUceniks(int id, Uceniks uceniks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != uceniks.Id)
            {
                return BadRequest();
            }

            _db.Entry(uceniks).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UceniksExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Uceniks
        [ResponseType(typeof(Uceniks))]
        public IHttpActionResult PostUceniks(Uceniks uceniks)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Uceniks.Add(uceniks);

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UceniksExists(uceniks.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = uceniks.Id }, uceniks);
        }

        // DELETE: api/Uceniks/5
        [ResponseType(typeof(Uceniks))]
        public IHttpActionResult DeleteUceniks(int id)
        {
            Uceniks uceniks = _db.Uceniks.Find(id);
            if (uceniks == null)
            {
                return NotFound();
            }

            _db.Uceniks.Remove(uceniks);
            _db.SaveChanges();

            return Ok(uceniks);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UceniksExists(int id)
        {
            return _db.Uceniks.Count(e => e.Id == id) > 0;
        }
    }
}