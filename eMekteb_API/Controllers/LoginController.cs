﻿using PeP_API.Models;
using PeP_API.VMs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PeP_API.Controllers
{
    public class LoginController : ApiController
    {
        db_entities _db = new db_entities();

        [HttpPost]
        [Route("api/Login")]
        public IHttpActionResult Login(LoginVM loginData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var userData = _db.Osobas.Where(x => x.Email == loginData.Email).FirstOrDefault();

                if (userData != null)
                {
                    if (!string.IsNullOrEmpty(userData.LozinkaSalt))
                    {
                        if (PeP_PCL.Util.UIHelper.GenerateHash(loginData.Password, userData.LozinkaSalt) == userData.LozinkaHash)
                        {
                            userData.Lozinka = userData.LozinkaHash = userData.LozinkaSalt = null;
                            return Ok(userData);
                        }
                    }

                    if (userData.Lozinka == loginData.Password)
                    {
                        userData.Lozinka = userData.LozinkaHash = userData.LozinkaSalt = null;
                        return Ok(userData);
                    }
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
