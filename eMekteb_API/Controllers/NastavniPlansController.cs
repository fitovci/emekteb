﻿using System;
using PeP_API.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace PeP_API.Controllers
{
    public class NastavniPlansController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/NastavniPlans
        public IQueryable<NastavniPlans> GetNastavniPlans()
        {
            return _db.NastavniPlans.Include(x=>x.Lekcijas).OrderByDescending(x=>x.Id);
        }

        // GET: api/NastavniPlans/5
        [ResponseType(typeof(NastavniPlans))]
        public IHttpActionResult GetNastavniPlans(int id)
        {
            try
            {
                NastavniPlans nastavniPlans = _db.NastavniPlans.Find(id);
                if (nastavniPlans == null)
                {
                    return NotFound();
                }

                return Ok(nastavniPlans);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/NastavniPlans/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNastavniPlans(int id, NastavniPlans nastavniPlans)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nastavniPlans.Id)
            {
                return BadRequest();
            }

            _db.Entry(nastavniPlans).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NastavniPlansExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/NastavniPlans
        [ResponseType(typeof(NastavniPlans))]
        public IHttpActionResult PostNastavniPlans(NastavniPlans nastavniPlans)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.NastavniPlans.Add(nastavniPlans);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nastavniPlans.Id }, nastavniPlans);
        }

        // DELETE: api/NastavniPlans/5
        [HttpGet]
        [ResponseType(typeof(NastavniPlans))]
        [Route("api/NastavniPlans/delete/{id}")]
        public IHttpActionResult DeleteNastavniPlans(int id)
        {
            NastavniPlans nastavniPlans = _db.NastavniPlans.Find(id);
            if (nastavniPlans == null)
            {
                return NotFound();
            }

            _db.NastavniPlans.Remove(nastavniPlans);
            _db.SaveChanges();

            return Ok(nastavniPlans);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NastavniPlansExists(int id)
        {
            return _db.NastavniPlans.Count(e => e.Id == id) > 0;
        }
    }
}