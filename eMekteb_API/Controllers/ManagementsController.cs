﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs;

namespace PeP_API.Controllers
{
    public class ManagementsController : ApiController
    {

        //// GET: api/Managements
        //public List<ManagementVM> GetManagements()
        //{
        //    return db.Managements.Select(x => new ManagementVM {
        //        ManagementId = x.Id,
        //        OsobaId = x.OsobaId,
        //        Ime = x.Osobas.Ime,
        //        Prezime = x.Osobas.Prezime,
        //        Adresa = x.Osobas.Adresa,
        //        DatumRodjenja = x.Osobas.DatumRodjenja,
        //        Email = x.Osobas.Email,
        //        Kontakt = x.Osobas.Kontakt,
        //        KorisnickoIme = x.Osobas.KorisnickoIme,
        //        Medzlis = x.Medzlis.Naziv
        //    }).ToList();
        //}

        //// GET: api/Managements/5
        //[ResponseType(typeof(Managements))]
        //public IHttpActionResult GetManagements(int id)
        //{
        //    Managements managements = db.Managements.Find(id);
            
        //    if (managements == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(managements);
        //}

        //// GET: api/Managements/5
        //[Route("api/Managements/GetManagementsById/{id}")]
        //public IHttpActionResult GetManagementsById(int id)
        //{
        //    var radnik = db.ups_GetManagementById(id).FirstOrDefault();

        //    if (radnik == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(radnik);
        //}

        //// PUT: api/Managements/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutManagements(int id, ManagementVM m)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != m.ManagementId)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {
        //        db.usp_PutManagement(m.ManagementId, m.OsobaId, m.Ime, m.Prezime, m.Kontakt,
        //                          m.Adresa, m.Email, m.DatumRodjenja, m.MedzlisId, m.KorisnickoIme);
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ManagementsExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Managements
        //[ResponseType(typeof(Managements))]
        //public IHttpActionResult PostManagements(Managements managements)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Managements.Add(managements);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = managements.Id }, managements);
        //}

        //// DELETE: api/Managements/5
        //[Route("api/Managements/Delete/{id}")]
        //[ResponseType(typeof(Managements))]
        //public IHttpActionResult DeleteManagements(int id)
        //{
        //    Managements managements = db.Managements.Find(id);
        //    if (managements == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Managements.Remove(managements);
        //    db.SaveChanges();

        //    return Ok(managements);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ManagementsExists(int id)
        //{
        //    return db.Managements.Count(e => e.Id == id) > 0;
        //}
    }
}