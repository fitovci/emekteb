﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;
using PeP_PCL.VMs;

namespace PeP_API.Controllers
{
    public class EvidencijaNapredovanjasController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/EvidencijaNapredovanjas
        public IHttpActionResult GetEvidencijaNapredovanjas()
        {
            try
            {
                var data = _db.EvidencijaNapredovanjas;
                if (data == null)
                {
                    return NotFound();
                }

                return Ok(data); 
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Route("api/EvidencijaNapredovanjas/GetPolozeneLekcije/{polaznikId}")]
        public IHttpActionResult GetPolozeneLekcije(int polaznikId)
        {
            try
            {
                List<NapredovanjeVM> lekcije =
                    _db.EvidencijaNapredovanjas.Where(d => d.PolaznikId == polaznikId)
                        .Select(x => new NapredovanjeVM
                        {
                            Id = x.Id,
                            Datum = x.Nastavas.Datum.ToString(),
                            Lekcija = x.Lekcijas.Naziv,
                            Ocjena = x.Ocjena
                        })
                        .ToList();

                DateTime temp;
                for (int i = 0; i < lekcije.Count; i++)
                {
                    lekcije[i].RedniBroj = i + 1;
                    temp = Convert.ToDateTime(lekcije[i].Datum);
                    lekcije[i].Datum = temp.ToString("D");
                }

                return Ok(lekcije.OrderByDescending(x => x.Id).ToList());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/EvidencijaNapredovanjas/5
        [ResponseType(typeof(EvidencijaNapredovanjas))]
        public IHttpActionResult GetEvidencijaNapredovanjas(int id)
        {
            EvidencijaNapredovanjas evidencijaNapredovanjas = _db.EvidencijaNapredovanjas.Find(id);
            if (evidencijaNapredovanjas == null)
            {
                return NotFound();
            }

            return Ok(evidencijaNapredovanjas);
        }

        // PUT: api/EvidencijaNapredovanjas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEvidencijaNapredovanjas(int id, EvidencijaNapredovanjas evidencijaNapredovanjas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != evidencijaNapredovanjas.Id)
            {
                return BadRequest();
            }

            _db.Entry(evidencijaNapredovanjas).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EvidencijaNapredovanjasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EvidencijaNapredovanjas
        [ResponseType(typeof(EvidencijaNapredovanjas))]
        public IHttpActionResult PostEvidencijaNapredovanjas(EvidencijaNapredovanjas evidencijaNapredovanjas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.EvidencijaNapredovanjas.Add(evidencijaNapredovanjas);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = evidencijaNapredovanjas.Id }, evidencijaNapredovanjas);
        }

        // DELETE: api/EvidencijaNapredovanjas/5
        [ResponseType(typeof(EvidencijaNapredovanjas))]
        public IHttpActionResult DeleteEvidencijaNapredovanjas(int id)
        {
            EvidencijaNapredovanjas evidencijaNapredovanjas = _db.EvidencijaNapredovanjas.Find(id);
            if (evidencijaNapredovanjas == null)
            {
                return NotFound();
            }

            _db.EvidencijaNapredovanjas.Remove(evidencijaNapredovanjas);
            _db.SaveChanges();

            return Ok(evidencijaNapredovanjas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EvidencijaNapredovanjasExists(int id)
        {
            return _db.EvidencijaNapredovanjas.Count(e => e.Id == id) > 0;
        }
    }
}