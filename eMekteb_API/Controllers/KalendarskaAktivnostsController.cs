﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs;
using PeP_API.Models;
using System.Data.Entity;

namespace PeP_API.Controllers
{
    public class KalendarskaAktivnostsController : ApiController
    {
        db_entities _db = new db_entities();

        // GET: api/KalendarskaAktivnosts/5
        [ResponseType(typeof(KalendarskaAktivnosts))]
        public IHttpActionResult GetKalendarskaAktivnosts()
        {
            try
            {
                IQueryable<KalendarskaAktivnosts> kalendarskaAktivnosts = _db.KalendarskaAktivnosts.OrderByDescending(x => x.Datum);

                return Ok(kalendarskaAktivnosts);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/KalendarskaAktivnosts/GetAktivnostsByGodina/{godina}/{isCurrent}")]
        public IHttpActionResult GetAktivnostsByGodina(int godina, bool isCurrent)
        {
            try
            {
                if (godina == 0 && isCurrent)
                    godina = new Helpers.SkolskaGodinaManager().GetCurrentSkolskaGodinaId();

                IQueryable<KalendarskaAktivnosts> kalendarskaAktivnosts = _db.KalendarskaAktivnosts.OrderByDescending(x => x.Datum).Where(x => x.SkolskaGodinaId == godina);

                return Ok(kalendarskaAktivnosts);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/KalendarskaAktivnosts/Search/{pocetak}/{kraj}")]
        public IHttpActionResult Search(DateTime pocetak, DateTime kraj)
        {
            try
            {
                var aktivnosti = _db.KalendarskaAktivnosts.Where(x => x.Datum >= pocetak && x.Datum <= kraj).OrderByDescending(x => x.Datum).ToList();

                return Ok(aktivnosti);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/KalendarskaAktivnosts/GetRangeOfDates")]
        public List<DateTime> GetRangeOfDates()
        {
            DateTime pocetak = _db.KalendarskaAktivnosts.OrderBy(x => x.Datum).FirstOrDefault().Datum;
            DateTime kraj = _db.KalendarskaAktivnosts.OrderByDescending(x => x.Datum).FirstOrDefault().Datum;

            return new List<DateTime>() { pocetak, kraj };
        }

        // GET: api/KalendarskaAktivnosts/5
        [ResponseType(typeof(KalendarskaAktivnosts))]
        public IHttpActionResult GetKalendarskaAktivnosts(int id)
        {
            try
            {
                KalendarskaAktivnosts kalendarskaAktivnosts = _db.KalendarskaAktivnosts.Include("skolskaGodinas").FirstOrDefault(x => x.Id == id);
                if (kalendarskaAktivnosts == null)
                {
                    return NotFound();
                }

                return Ok(kalendarskaAktivnosts);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/KalendarskaAktivnosts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKalendarskaAktivnosts(int id, KalendarskaAktivnosts kalendarskaAktivnosts)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != kalendarskaAktivnosts.Id)
            {
                return BadRequest();
            }

            _db.Entry(kalendarskaAktivnosts).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KalendarskaAktivnostsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KalendarskaAktivnosts
        [ResponseType(typeof(KalendarskaAktivnosts))]
        public IHttpActionResult PostKalendarskaAktivnosts(KalendarskaAktivnosts kalendarskaAktivnosts)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.KalendarskaAktivnosts.Add(kalendarskaAktivnosts);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = kalendarskaAktivnosts.Id }, kalendarskaAktivnosts);
        }

        [HttpGet]
        // DELETE: api/KalendarskaAktivnosts/5
        [ResponseType(typeof(KalendarskaAktivnosts))]
        [Route("api/KalendarskaAktivnosts/delete/{id}")]
        public IHttpActionResult DeleteKalendarskaAktivnosts(int id)
        {
            KalendarskaAktivnosts kalendarskaAktivnosts = _db.KalendarskaAktivnosts.Find(id);
            if (kalendarskaAktivnosts == null)
            {
                return NotFound();
            }

            _db.KalendarskaAktivnosts.Remove(kalendarskaAktivnosts);
            _db.SaveChanges();

            return Ok(kalendarskaAktivnosts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KalendarskaAktivnostsExists(int id)
        {
            return _db.KalendarskaAktivnosts.Count(e => e.Id == id) > 0;
        }
    }
}