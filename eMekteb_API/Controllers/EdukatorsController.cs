﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs;
using PeP_API.Models;

namespace PeP_API.Controllers
{
    public class EdukatorsController : ApiController
    {
        db_entities _db = new db_entities();

        //GET: api/Edukators
        public IHttpActionResult GetEdukators()
        {
            try
            {
                var edukatori = _db.Edukators;
                foreach (var edukator in edukatori)
                {
                    edukator.Osobas = _db.Osobas.FirstOrDefault(x => x.Id == edukator.OsobaId);
                    edukator.Mektebs = _db.Mektebs.FirstOrDefault(x => x.EdukatorId == edukator.Id);
                }

                return Ok(edukatori);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [ResponseType(typeof(List<EdukatorVM>))]
        [Route("api/Edukators/GetEdukatorByUsername/{username}")]
        public IHttpActionResult GetEdukatorByUsername(string username)
        {
            try
            {
                List<EdukatorVM> edukatori = _db.Edukators.Select(x => new EdukatorVM
                {
                    EdukatorId = x.Id,
                    OsobaId = x.OsobaId,
                    MektebId = x.MektebId,
                    Mekteb = x.Mektebs
                }).ToList();

                foreach (EdukatorVM edukators in edukatori)
                {
                    var o = _db.Osobas.FirstOrDefault(x => x.Id == edukators.OsobaId);

                    edukators.Ime = o.Ime;
                    edukators.Prezime = o.Prezime;
                    edukators.Kontakt = o.Kontakt;
                    edukators.Adresa = o.Adresa;
                    edukators.Email = o.Email;
                    edukators.DatumRodjenja = o.DatumRodjenja;
                    edukators.KorisnickoIme = o.KorisnickoIme;
                    edukators.SlikaThumb = o.SlikaThumb;
                }

                if (edukatori == null)
                {
                    return NotFound();
                }

                return Ok(edukatori.Where(x => x.KorisnickoIme.Contains(username) == true));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Edukators/5
        [ResponseType(typeof(EdukatorVM))]
        public IHttpActionResult GetEdukators(int id)
        {
            try
            {
                EdukatorVM edukators = _db.Edukators.Select(x => new EdukatorVM
                {
                    EdukatorId = x.Id,
                    OsobaId = x.OsobaId,
                    MektebId = x.MektebId,
                    Mekteb = x.Mektebs
                }).FirstOrDefault(x => x.EdukatorId == id);

                Osobas o = _db.Osobas.FirstOrDefault(x => x.Id == edukators.OsobaId);

                if (edukators != null)
                {
                    if (o != null)
                    {
                        edukators.Ime = o.Ime;
                        edukators.Prezime = o.Prezime;
                        edukators.Kontakt = o.Kontakt;
                        edukators.Adresa = o.Adresa;
                        edukators.Email = o.Email;
                        edukators.DatumRodjenja = o.DatumRodjenja;
                        edukators.KorisnickoIme = o.KorisnickoIme;
                        edukators.SlikaThumb = o.SlikaThumb;
                    }
                    else
                        return NotFound();
                }
                else
                    return NotFound();

                return Ok(edukators);

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Edukators/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEdukators(int id, EdukatorVM edukators)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != edukators.EdukatorId)
            {
                return BadRequest();
            }

            try
            {
                _db.usp_PutEdukator(edukators.EdukatorId, edukators.OsobaId, edukators.Ime, edukators.Prezime, edukators.Kontakt,
                                   edukators.Adresa, edukators.Email, edukators.DatumRodjenja, edukators.MektebId, edukators.KorisnickoIme);

                if (edukators.SlikaThumb != null && edukators.Slika != null)
                    _db.usp_PutProfilePhoto(edukators.OsobaId, edukators.SlikaThumb, edukators.Slika);

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EdukatorsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Edukators
        [ResponseType(typeof(Edukators))]
        public IHttpActionResult PostEdukators(EdukatorVM edukators)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int getId = _db.Edukators.OrderByDescending(x => x.Id).FirstOrDefault().Id;

            try
            {
                Osobas osoba = new Osobas()
                {
                    Ime = edukators.Ime,
                    Prezime = edukators.Prezime,
                    KorisnickoIme = edukators.KorisnickoIme,
                    Kontakt = edukators.Kontakt,
                    Adresa = edukators.Adresa,
                    Email = edukators.Email,
                    DatumRodjenja = edukators.DatumRodjenja
                };

                _db.Osobas.Add(osoba);
                _db.SaveChanges();

                var lastOsoba = _db.Osobas.OrderByDescending(x => x.Id).FirstOrDefault();

                _db.usp_PostEdukators(getId + 1, lastOsoba.Id, edukators.MektebId);
            }
            catch (DbUpdateException)
            {
                if (EdukatorsExists(edukators.EdukatorId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = edukators.EdukatorId }, edukators);
        }

        // DELETE: api/Edukators/5
        [Route("api/Edukators/Delete/{id}")]
        [ResponseType(typeof(Edukators))]
        public IHttpActionResult DeleteEdukators(int id)
        {
            Edukators edukators = _db.Edukators.Find(id);
            if (edukators == null)
            {
                return NotFound();
            }

            _db.Edukators.Remove(edukators);
            _db.SaveChanges();

            return Ok(edukators);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EdukatorsExists(int id)
        {
            return _db.Edukators.Count(e => e.Id == id) > 0;
        }
    }
}