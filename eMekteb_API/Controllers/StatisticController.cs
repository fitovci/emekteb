﻿using PeP_API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.VMs;

namespace PeP_API.Controllers
{
    public class StatisticController : ApiController
    {
        private db_entities _db = new db_entities();

        [HttpGet]
        [Route("api/Statistic/GetAktivnostiStatistics")]
        public IHttpActionResult GetAktivnostiStatistics()
        {
            
            var mektebs = _db.Mektebs.Include(x => x.Razreds)
                                     .Include(x => x.Razreds.Select(y=>y.Polaznik))
                                     .Include(x => x.Razreds.Select(y=>y.Nastavas))
                                     .Include(x => x.Razreds.Select(y=>y.Nastavas.Select(z=>z.Polaznik)))
                                     .ToList();

            var polaznici = _db.Polaznik.Include(x => x.EvidencijaNapredovanjas)
                                      .Include(x => x.Uceniks)
                                      .Include(x => x.Uceniks.Osobas)
                                      .Include(x => x.Razreds.Mektebs)
                                      .ToList();

            var skolskeGodine = _db.SkolskaGodinas.Include(x => x.Razreds.Select(y => y.Polaznik)).ToList();

            // -------------------------------------------------------------------------------------------------------
            // tabela mekteba i porediti ih po broju ucenika
            // -------------------------------------------------------------------------------------------------------

            var mekteb_brojUcenika = new Dictionary<string, int>();
            foreach (var mekteb in mektebs)
            {
                var brojUcenika = mekteb.Razreds.Sum(razred => razred.Polaznik.Count());
                mekteb_brojUcenika.Add(mekteb.Naziv, brojUcenika);
            }

            var sortiraniMektebiPoBrojuUcenika = new List<KeyValuePair<string, int>>();
            if(mekteb_brojUcenika.Any())
                sortiraniMektebiPoBrojuUcenika = mekteb_brojUcenika.ToList().OrderByDescending(x=>x.Value).Take(5).ToList();
            

            // -------------------------------------------------------------------------------------------------------
            // tabela mekteba sa najvecim postotkom prisustva
            // -------------------------------------------------------------------------------------------------------

            var mekteb_prisustvo = new Dictionary<string, decimal>();
            foreach (var mekteb in mektebs)
            {
                var postotci = new List<decimal>();
                foreach (var razred in mekteb.Razreds)
                {
                    var brojUcenika = razred.Polaznik.Count();
                    foreach (var cas in razred.Nastavas)
                    {
                        var postotakPrisustvaNaCasu = decimal.Round(((decimal)cas.Polaznik.Count / brojUcenika) * 100, 3);
                        postotci.Add(postotakPrisustvaNaCasu);
                    }
                }
                mekteb_prisustvo.Add(mekteb.Naziv, postotci.Any() ? decimal.Round(postotci.Average(), 2) : decimal.Zero);
            }

            var sortiraniMektebiPoPrisustvu = new List<KeyValuePair<string, decimal>>();
            if (mekteb_prisustvo.Any())
                sortiraniMektebiPoPrisustvu = mekteb_prisustvo.ToList().OrderByDescending(x => x.Value).Take(5).ToList();

            // -------------------------------------------------------------------------------------------------------
            // ucenici sa najvecim prosjekom ocjena i brojem prisutva
            // -------------------------------------------------------------------------------------------------------

            var ucenik_prosjekOcjena = new Dictionary<string, decimal>();

            foreach (var polaznik in polaznici)
            {
                if (polaznik.EvidencijaNapredovanjas.Count > 0)
                {
                    var ocjene_sum = polaznik.EvidencijaNapredovanjas.Sum(evidencija => evidencija.Ocjena);
                    var prosjek = decimal.Round((decimal)ocjene_sum / polaznik.EvidencijaNapredovanjas.Count, 2);
                    ucenik_prosjekOcjena.Add(
                            polaznik.Uceniks.Osobas.Ime + " " + polaznik.Uceniks.Osobas.Prezime + ", " + polaznik.Razreds.Mektebs.Naziv,
                            prosjek
                        );
                }
            }

            var sortiraniUceniciPoProsjeku = new List<KeyValuePair<string, decimal>>();
            if (ucenik_prosjekOcjena.Any())
                sortiraniUceniciPoProsjeku = ucenik_prosjekOcjena.ToList().OrderByDescending(x => x.Value).Take(5).ToList();

            // -------------------------------------------------------------------------------------------------------
            // porediti skolske godine po broju ucenika
            // -------------------------------------------------------------------------------------------------------

            var skolskeGodine_brojUcenika = new Dictionary<string, int>();
            foreach (var skolskaGodina in skolskeGodine)
            {
                skolskeGodine_brojUcenika.Add(skolskaGodina.Naziv, skolskaGodina.Razreds.Sum(x=>x.Polaznik.Count));
            }

            var sortiraneGodinePoUcenicima = new List<KeyValuePair<string, int>>();
            if (skolskeGodine_brojUcenika.Any())
                sortiraneGodinePoUcenicima = skolskeGodine_brojUcenika.ToList().OrderByDescending(x => x.Value).Take(5).ToList();


            StatisticsVM statistics = new StatisticsVM
            {
                MektebiPoBrojuUcenika = sortiraniMektebiPoBrojuUcenika,
                MektebiPoPrisustvu = sortiraniMektebiPoPrisustvu,
                UceniciPoProsjeku = sortiraniUceniciPoProsjeku,
                SkolskeGodinePoUcenicima = sortiraneGodinePoUcenicima
            };

            return Ok(statistics);
        }

        [HttpGet]
        [Route("api/Statistic/GetCounters")]
        public IHttpActionResult GetCounters()
        {
            var muftijstva = _db.Muftijstvoes.Select(x => x.Naziv).ToList();
            var medzlisi = _db.Medzlis.Select(x => x.Naziv).ToList();
            var mektebs = _db.Mektebs.Select(x => x.Naziv).ToList();
            var brojEdukatora = _db.Edukators.Count();
            var brojPolaznika = _db.Uceniks.Count();

            var statistics = new CountersVM()
            {
                Medzlisi = medzlisi,
                Mektebi = mektebs,
                Muftijstva = muftijstva,
                BrojEdukatora = brojEdukatora,
                BrojPolaznika = brojPolaznika,
                BrojMedzlisa = medzlisi.Count,
                BrojMekteba = mektebs.Count
            };

            return Ok(statistics);

        }
    }
}
