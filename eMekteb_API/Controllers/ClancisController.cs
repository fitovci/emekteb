﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_PCL.VMs;

namespace PeP_API.Controllers
{
    public class ClancisController : ApiController
    {

        //[HttpGet]
        //[Route("api/Clancis/GetPreporuka/{razredId}/{osobaId}")]
        //public List<ClanciVM> GetPreporuka(int razredId, int osobaId)
        //{
        //    Recommender r = new Recommender();

        //    List<Clanci> c = r.GetPreporuka(razredId, osobaId);

        //    List<ClanciVM> preporuceni = c.Select(x => new ClanciVM
        //    {
        //        Id = x.Id,
        //        Datum = x.Datum.ToString(),
        //        Naziv = x.Naslov,
        //        Sadrzaj = x.Sadrzaj
        //    }).ToList();

        //    return preporuceni;
        //}

        //// GET: api/Clancis
        //public List<ClanciVM> GetClanci()
        //{
        //    List<ClanciVM> clanci = db.Clanci.OrderByDescending(x=>x.Datum).Select(x => new ClanciVM
        //    {
        //        Id = x.Id,
        //        Datum = x.Datum.ToString(),
        //        Naziv = x.Naslov,
        //        Sadrzaj = x.Sadrzaj,
        //        Autor = x.Edukators.Osobas.Ime + " " + x.Edukators.Osobas.Prezime
        //    }).ToList();

        //    return clanci;
        //}

        //[Route("api/Clancis/GetClanciSearch/{naslov?}")]
        //public List<ClanciVM> GetClanciSearch(string naslov = "")
        //{
        //    List<ClanciVM> clanci;
        //    if (naslov == "")
        //    {
        //        clanci = db.Clanci.OrderByDescending(x => x.Datum).Select(x => new ClanciVM
        //        {
        //            Id = x.Id,
        //            Datum = x.Datum.ToString(),
        //            Naziv = x.Naslov,
        //            Sadrzaj = x.Sadrzaj,
        //            Autor = x.Edukators.Osobas.Ime + " " + x.Edukators.Osobas.Prezime
        //        }).ToList();
        //    }
        //    else
        //    {
        //        clanci = db.Clanci.Where(x=>x.Naslov.Contains(naslov)).OrderByDescending(x => x.Datum).Select(x => new ClanciVM
        //        {
        //            Id = x.Id,
        //            Datum = x.Datum.ToString(),
        //            Naziv = x.Naslov,
        //            Sadrzaj = x.Sadrzaj,
        //            Autor = x.Edukators.Osobas.Ime + " " + x.Edukators.Osobas.Prezime
        //        }).ToList();
        //    }

        //    return clanci;
        //}

        //// GET: api/Clancis/5
        //[ResponseType(typeof(ClanciVM))]
        //public IHttpActionResult GetClanci(int id)
        //{
        //    Clanci clanci = db.Clanci.Find(id);
        //    if (clanci == null)
        //    {
        //        return NotFound();
        //    }
        //    string e =
        //        db.Edukators.Where(x => x.Id == clanci.EdukatorId)
        //            .Select(x => x.Osobas.Ime +" "+ x.Osobas.Prezime)
        //            .FirstOrDefault();

        //    ClanciVM c = new ClanciVM()
        //    {
        //        Id = clanci.Id,
        //        Datum = clanci.Datum.ToString(),
        //        Naziv = clanci.Naslov,
        //        Sadrzaj = clanci.Sadrzaj,
        //        Autor = e
        //    };

        //    return Ok(c);
        //}

        //// PUT: api/Clancis/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutClanci(int id, Clanci clanci)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != clanci.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(clanci).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ClanciExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/Clancis
        //[ResponseType(typeof(Clanci))]
        //public IHttpActionResult PostClanci(Clanci clanci)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Clanci.Add(clanci);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = clanci.Id }, clanci);
        //}

        //// DELETE: api/Clancis/5
        //[ResponseType(typeof(Clanci))]
        //public IHttpActionResult DeleteClanci(int id)
        //{
        //    Clanci clanci = db.Clanci.Find(id);
        //    if (clanci == null)
        //    {
        //        return NotFound();
        //    }

        //    db.Clanci.Remove(clanci);
        //    db.SaveChanges();

        //    return Ok(clanci);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ClanciExists(int id)
        //{
        //    return db.Clanci.Count(e => e.Id == id) > 0;
        //}
    }
}