﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;
using PeP_PCL.VMs;

namespace PeP_API.Controllers
{
    public class ZadacasController : ApiController
    {
        private db_entities db = new db_entities();

        // GET: api/Zadacas
        public IQueryable<Zadacas> GetZadacas()
        {
            return db.Zadacas;
        }

        [HttpGet]
        [Route("api/Zadacas/GetZadaceByPolaznik/{polaznikId}")]
        public IHttpActionResult GetZadaceByPolaznik(int polaznikId)
        {
            try
            {
                List<ZadacaVM> zadace =
                    db.Zadacas.Where(x => x.PolaznikId == polaznikId).Select(x => new ZadacaVM
                    {
                        Id = x.Id,
                        Lekcija = x.Lekcijas.Naziv,
                        Datum = x.Nastavas.Datum.ToString(),
                        Zavrsena = x.Zavrsena
                    }).OrderByDescending(x => x.Id).ToList();

                foreach (ZadacaVM t in zadace)
                {
                    var temp = Convert.ToDateTime(t.Datum);
                    t.Datum = temp.ToString("D");
                }

                return Ok(zadace);
            }
            catch (Exception e)
            {
               return InternalServerError(e);
            }
        }

        // GET: api/Zadacas/5
        [ResponseType(typeof(Zadacas))]
        public IHttpActionResult GetZadacas(int id)
        {
            Zadacas zadacas = db.Zadacas.Find(id);
            if (zadacas == null)
            {
                return NotFound();
            }

            return Ok(zadacas);
        }

        // PUT: api/Zadacas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutZadacas(int id, Zadacas zadacas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zadacas.Id)
            {
                return BadRequest();
            }

            db.Entry(zadacas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ZadacasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Zadacas
        [ResponseType(typeof(Zadacas))]
        public IHttpActionResult PostZadacas(Zadacas zadacas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Zadacas.Add(zadacas);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zadacas.Id }, zadacas);
        }

        // DELETE: api/Zadacas/5
        [ResponseType(typeof(Zadacas))]
        public IHttpActionResult DeleteZadacas(int id)
        {
            Zadacas zadacas = db.Zadacas.Find(id);
            if (zadacas == null)
            {
                return NotFound();
            }

            db.Zadacas.Remove(zadacas);
            db.SaveChanges();

            return Ok(zadacas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ZadacasExists(int id)
        {
            return db.Zadacas.Count(e => e.Id == id) > 0;
        }
    }
}