﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;

namespace PeP_API.Controllers
{
    public class NastavasController : ApiController
    {
        private db_entities db = new db_entities();

        // GET: api/Nastavas
        public IQueryable<Nastavas> GetNastavas()
        {
            return db.Nastavas;
        }

        // GET: api/Nastavas/5
        [ResponseType(typeof(Nastavas))]
        public IHttpActionResult GetNastavas(int id)
        {
            Nastavas nastavas = db.Nastavas.Find(id);
            if (nastavas == null)
            {
                return NotFound();
            }

            return Ok(nastavas);
        }

        // PUT: api/Nastavas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNastavas(int id, Nastavas nastavas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != nastavas.Id)
            {
                return BadRequest();
            }

            db.Entry(nastavas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NastavasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Nastavas
        [ResponseType(typeof(Nastavas))]
        public IHttpActionResult PostNastavas(Nastavas nastavas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Nastavas.Add(nastavas);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = nastavas.Id }, nastavas);
        }

        // DELETE: api/Nastavas/5
        [ResponseType(typeof(Nastavas))]
        public IHttpActionResult DeleteNastavas(int id)
        {
            Nastavas nastavas = db.Nastavas.Find(id);
            if (nastavas == null)
            {
                return NotFound();
            }

            db.Nastavas.Remove(nastavas);
            db.SaveChanges();

            return Ok(nastavas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NastavasExists(int id)
        {
            return db.Nastavas.Count(e => e.Id == id) > 0;
        }
    }
}