﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using PeP_API.Models;
using PeP_PCL.VMs;
namespace PeP_API.Controllers
{
    public class OsobasController : ApiController
    {
        db_entities _db = new db_entities();

        [HttpGet]
        [Route("api/Osobas/GetOsobasForOsobaVM")]
        public IHttpActionResult GetOsobaByName()
        {
            List<OsobaVM> osobas = _db.Osobas.OrderBy(x => x.Ime).ThenBy(x => x.Prezime).Select(x => new OsobaVM
            {
                Id = x.Id,
                ImePrezime = x.Ime + " " + x.Prezime
            }).ToList();


            if (osobas == null)
                return NotFound();

            return Ok(osobas);
        }

        [ResponseType(typeof(Osobas))]
        [Route("api/Osobas/GetOsobaByUsername/{username}")]
        public IHttpActionResult GetOsobaByUsername(string username)
        {
            try
            {
                Osobas osobas = _db.Osobas.FirstOrDefault(x => x.KorisnickoIme == username);
                if (osobas == null)
                    return NotFound();

                return Ok(osobas);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/Osobas/GetProfilePhotoByOsoba/{osobaId}")]
        public IHttpActionResult GetProfilePhotoByOsoba(int osobaId)
        {
            try
            {
                SlikaVM profilna = _db.Osobas.Where(x => x.Id == osobaId).Select(x => new SlikaVM
                {
                    SlikaThumb = x.SlikaThumb,
                    Slika = x.Slika
                }).FirstOrDefault();

                if (profilna == null)
                {
                    return NotFound();
                }

                return Ok(profilna);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [ResponseType(typeof(int))]
        [Route("api/Osobas/GetOsobaIdByNameByDatum/{name}/{date}")]
        public IHttpActionResult GetOsobaIdByNameByDatum(string name, string date)
        {
            try
            {
                string ime = name.Substring(0, name.IndexOf(" ")).Trim();
                string prezime = name.Substring(name.IndexOf(" ") + 1, name.Length - name.IndexOf(" ") - 1).Trim();
                DateTime datum = Convert.ToDateTime(date);

                var osoba = _db.Osobas.FirstOrDefault(x => x.Ime == ime && x.Prezime == prezime && x.DatumRodjenja == datum);
                if (osoba != null)
                {
                    int osobasId = osoba.Id;

                    return Ok(osobasId);
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // GET: api/Osobas
        public IQueryable<Osobas> GetOsobas()
        {
            return _db.Osobas;
        }

        [HttpGet]
        [Route("api/Osobas/GetOsobasForConversation")]
        public IQueryable<OsobaVM> GetOsobasForConversation()
        {
            return _db.Osobas.Select(x=> new OsobaVM()
            {
                Id = x.Id, 
                ImePrezime = x.Ime + " " + x.Prezime
            }).OrderBy(x=>x.ImePrezime);
        }

        // GET: api/Osobas/5
        [ResponseType(typeof(Osobas))]
        public IHttpActionResult GetOsobas(int id)
        {
            try
            {
                Osobas osobas = _db.Osobas.Find(id);
                if (osobas == null)
                {
                    return NotFound();
                }

                return Ok(osobas);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PUT: api/Osobas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOsobas(int id, Osobas osobas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != osobas.Id)
            {
                return BadRequest();
            }

            _db.Entry(osobas).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OsobasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Osobas
        [ResponseType(typeof(Osobas))]
        public IHttpActionResult PostOsobas(Osobas osobas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Osobas.Add(osobas);
            _db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = osobas.Id }, osobas);
        }

        // DELETE: api/Osobas/5
        [Route("api/Osobas/Delete/{id}")]
        [ResponseType(typeof(Osobas))]
        public IHttpActionResult DeleteOsobas(int id)
        {
            Osobas osobas = _db.Osobas.Find(id);
            if (osobas == null)
            {
                return NotFound();
            }

            _db.Osobas.Remove(osobas);
            _db.SaveChanges();

            return Ok(osobas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OsobasExists(int id)
        {
            return _db.Osobas.Count(e => e.Id == id) > 0;
        }
    }
}