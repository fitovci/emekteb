﻿using PeP_API.Models;
using PeP_API.VMs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.Util
{
    public class Autentifikacija
    {
        private const string LogiraniKorisnik = "logirani_korisnik";

        public static void PokreniNovuSesiju(LogiraniKorisnikVM korisnik, HttpContextBase context, bool zapamtiPassword)
        {
            context.Session.Add(LogiraniKorisnik, korisnik);

            if (zapamtiPassword)
            {
                HttpCookie cookie = new HttpCookie("_mvc_session", korisnik != null ? korisnik.Id.ToString() : "");
                cookie.Expires = DateTime.Now.AddDays(30);
                context.Response.Cookies.Add(cookie);
            }
        }

        public static LogiraniKorisnikVM GetLogiraniKorisnik(HttpContextBase context)
        {
            LogiraniKorisnikVM korisnik = (LogiraniKorisnikVM)context.Session[LogiraniKorisnik];

            return korisnik;
        }
    }
}