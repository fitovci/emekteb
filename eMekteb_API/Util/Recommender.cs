﻿using PeP_API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace eMekteb_API.Util
{
    public class Recommender
    {

        Dictionary<int, List<ClanciOcjene>> Ocjene = new Dictionary<int, List<ClanciOcjene>>();

        Dictionary<int, double> ProsjecneOcjene = new Dictionary<int, double>();

        private List<ClanciOcjene> MojeOcjene = new List<ClanciOcjene>();

        List<double> Slicnosti = new List<double>();

        private List<int> osobeIzRazreda = new List<int>();


        private db_entities _db = new db_entities();

        private void GetOcjene(int razredId, int osobaId)
        {
            MojeOcjene = _db.ClanciOcjene.Where(x => x.OsobaId == osobaId).ToList();

            GetOsobasIdByRazred(razredId, osobaId);

            List<ClanciOcjene> ocjeneList = new List<ClanciOcjene>();

            foreach (int i in osobeIzRazreda)
            {
                ocjeneList = _db.ClanciOcjene.Where(x => x.OsobaId == i).ToList();
                Ocjene.Add(i, ocjeneList);
            }
        }
        private void GetOsobasIdByRazred(int razredId, int osobaId)
        {
            List<int> ucenikList =
                _db.Polaznik.Where(x => x.RazredId == razredId).Select(x => x.UcenikId).ToList();

            List<int> osobeIzRazreda = new List<int>();

            foreach (int i in ucenikList)
            {
                int id = _db.Uceniks.Where(x => x.Id == i).Select(x => x.Id).FirstOrDefault();

                if (id != osobaId)
                    osobeIzRazreda.Add(id);
            }
        }
        private void GetProsjecneOcjene()
        {
            foreach (var x in Ocjene)
            {
                ProsjecneOcjene.Add(x.Key, (double)(x.Value.Sum(y => y.Ocjena) / x.Value.Count));
            }
        }

        private double GetSlicnost(int osobaId)
        {
            List<ClanciOcjene> listaOcjena = Ocjene[osobaId];

            List<ClanciOcjene> zajednickeOcjene1 = new List<ClanciOcjene>();
            List<ClanciOcjene> zajednickeOcjene2 = new List<ClanciOcjene>();

            for (int i = 0; i < MojeOcjene.Count; i++)
            {
                for (int j = 0; j < listaOcjena.Count; j++)
                {
                    
                    if (MojeOcjene[i].ClanakId == listaOcjena[j].ClanakId)
                    {
                        zajednickeOcjene1.Add(MojeOcjene[i]);
                        zajednickeOcjene2.Add(listaOcjena[j]);
                    }
                }
            }

            return IzracunajSlicnost(zajednickeOcjene1, zajednickeOcjene2);
        }

        private double IzracunajSlicnost(List<ClanciOcjene> zajednickeOcjene1, List<ClanciOcjene> zajednickeOcjene2)
        {
            double prosjecnaOcjena1 = (double) (MojeOcjene.Sum(x => x.Ocjena)/MojeOcjene.Count);
            double prosjecnaOcjena2 = ProsjecneOcjene[zajednickeOcjene1[0].OsobaId];

            double brojnik = 0, nazivnik1 = 0, nazivnik2 = 0;

            for (int i = 0; i < zajednickeOcjene1.Count; i++)
            {
                brojnik += ((Double)zajednickeOcjene1[i].Ocjena - prosjecnaOcjena1)* ((Double)zajednickeOcjene2[i].Ocjena - prosjecnaOcjena2);

                nazivnik1 += ((Double) zajednickeOcjene1[i].Ocjena - prosjecnaOcjena1)*
                             ((Double) zajednickeOcjene1[i].Ocjena - prosjecnaOcjena1);

                nazivnik2 += ((Double) zajednickeOcjene2[i].Ocjena - prosjecnaOcjena2)*
                             ((Double) zajednickeOcjene2[i].Ocjena - prosjecnaOcjena2);
            }

            nazivnik1 = Math.Sqrt(nazivnik1);
            nazivnik2 = Math.Sqrt(nazivnik2);

            double slicnost = brojnik/(nazivnik1*nazivnik2);

            return slicnost;
        }

        public List<Clanci> GetPreporuka(int razredId, int osobaId)
        {
            GetOcjene(razredId, osobaId);
            GetProsjecneOcjene();

            foreach (int i in osobeIzRazreda)
            {
               Slicnosti.Add(GetSlicnost(i));  
            }
            
            
            List<int> najslicnijeosobe = new List<int>();
            for (int i = 0; i < Slicnosti.Count; i++)
            {
                if (Slicnosti[i] > 0.6)
                {
                    najslicnijeosobe.Add(osobeIzRazreda[i]);
                }
            }

            //uzeti clanke koje nisam ocjenio

            List<int> ocjenjeniClanci = MojeOcjene.Select(x => x.ClanakId).ToList();
            List<Clanci> neocjenjeniClanci = _db.Clanci.ToList();

            for (int i = 0; i < ocjenjeniClanci.Count; i++)
            {
                for (int j = 0; j < neocjenjeniClanci.Count; j++)
                {
                    if (neocjenjeniClanci[j].Id == ocjenjeniClanci[i])
                    {
                        neocjenjeniClanci.RemoveAt(j);
                    }
                }
            }


            Dictionary<int, double> pretpostavke = new Dictionary<int, double>();
            
            for (int i = 0; i < neocjenjeniClanci.Count; i++)
            {
                pretpostavke.Add(neocjenjeniClanci[i].Id, GetPretpostavka(neocjenjeniClanci[i], najslicnijeosobe));
            }

            List<int> preporukaId = pretpostavke.OrderByDescending(x => x.Value).Select(x => x.Key).Take(3).ToList();
            List<Clanci> preporuka = new List<Clanci>();

            foreach (int c in preporukaId)
            {
                preporuka.Add(neocjenjeniClanci.Where(x=>x.Id == c).FirstOrDefault());    
            }

            return preporuka;
        }

        private double GetPretpostavka(Clanci clanak, List<int> najslicnijeosobe)
        {
            double brojnik = 0, nazivnik = 0, suma = (double)(MojeOcjene.Sum(x => x.Ocjena) / MojeOcjene.Count);

            for (int i = 0; i < najslicnijeosobe.Count; i++)
            {
                brojnik += Slicnosti[najslicnijeosobe[i]]*
                           (Ocjene[najslicnijeosobe[i]].Where(x => x.ClanakId == clanak.Id)
                               .Select(x => x.Ocjena)
                               .FirstOrDefault()
                               .GetValueOrDefault() - ProsjecneOcjene[najslicnijeosobe[i]]);
                nazivnik += Slicnosti[najslicnijeosobe[i]];
            }

            suma += (brojnik/nazivnik);

            return suma;
        }
    }
}