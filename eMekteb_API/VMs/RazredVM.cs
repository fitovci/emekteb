﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class RazredVM
    {
        public int? Id { get; set; }
        public string Naziv { get; set; }
    }
}