﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class ManagementVM
    {
        public int ManagementId { get; set; }
        public int? OsobaId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Kontakt { get; set; }
        public string Adresa { get; set; }
        public string Email { get; set; }
        public System.DateTime DatumRodjenja { get; set; }
        public string KorisnickoIme { get; set; }
        public string Medzlis { get; set; }
        public int MedzlisId { get; set; }
    }
}