﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PeP_API.Models;

namespace PeP_API.VMs
{
    public class EdukatorVM
    {
        public int EdukatorId { get; set; }
        public int OsobaId { get; set; }
        public int? MektebId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Kontakt { get; set; }
        public string Adresa { get; set; }
        public string Email { get; set; }
        public System.DateTime DatumRodjenja { get; set; }
        public string KorisnickoIme { get; set; }
        public byte[] Slika { get; set; }
        public byte[] SlikaThumb { get; set; }
        public Mektebs Mekteb { get; set; }
    }
}