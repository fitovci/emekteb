﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class LekcijaVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
    }
}