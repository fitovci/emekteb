﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class AktivnostiVM
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Napomena { get; set; }
        public System.DateTime Datum { get; set; }
        public string SkolskaGodina { get; set; }
    }
}