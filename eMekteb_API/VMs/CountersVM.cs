﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class CountersVM
    {
        public int BrojMekteba { get; set; }
        public int BrojPolaznika { get; set; }
        public int BrojEdukatora { get; set; }
        public int BrojMedzlisa { get; set; }

        public List<string> Muftijstva { get; set; }
        public List<string> Medzlisi { get; set; }
        public List<string> Mektebi { get; set; }
    }
}