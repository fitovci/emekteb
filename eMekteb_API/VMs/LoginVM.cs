﻿using PeP_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class LoginVM
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}