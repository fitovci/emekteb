﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class StatisticsVM
    {
        public List<KeyValuePair<string, int>> MektebiPoBrojuUcenika { get; set; }
        public List<KeyValuePair<string, decimal>> MektebiPoPrisustvu { get; set; }
        public List<KeyValuePair<string, decimal>> UceniciPoProsjeku { get; set; }
        public List<KeyValuePair<string, int>> SkolskeGodinePoUcenicima { get; set; }
    }
}