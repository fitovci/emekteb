﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs
{
    public class rptKalendarVM
    {
        public string Naziv { get; set; }
        public string Napomena { get; set; }
        public string Datum { get; set; }
    }
}