﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.VMs.ReportsVMs
{
    public class rptStatistikaVM
    {
        public string Mekteb { get; set; }
        public int UpisanoDjece { get; set; }
    }
}