﻿using PeP_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeP_API.Helpers
{
    public class SkolskaGodinaManager
    {
        db_entities _db = new db_entities();

        public SkolskaGodinas GetCurrentSkolskaGodina()
        {
            return _db.SkolskaGodinas.FirstOrDefault(x => DateTime.Now.Year >= x.Pocinje.Year && DateTime.Now.Year <= x.Zavrsava.Year);
        }

        public int GetCurrentSkolskaGodinaId()
        {
            var firstOrDefault = _db.SkolskaGodinas
                .FirstOrDefault(x => DateTime.Now.Year >= x.Pocinje.Year && DateTime.Now.Year <= x.Zavrsava.Year);
            if (firstOrDefault != null)
                return firstOrDefault.Id;
            return 0;
        }
    }

}