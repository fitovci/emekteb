﻿
var ucenikApp = angular.module("ucenikApp", [
    'ngRoute',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngMessages',
    'ngAnimate'
]);

ucenikApp.constant("Config", {
    source: "http://localhost:5757/api/"
});

ucenikApp.run(function ($rootScope, $location, localStorageService) {
    var loggedUser = localStorageService.get('authorizationDataUcenik');
    if (!loggedUser || loggedUser === null || loggedUser === undefined) {
        $location.path('/login');
    }

    $rootScope.activeURL = $location;
});

ucenikApp.config(function ($routeProvider) {
    $routeProvider
        .when("/licniPodaci", {
            templateUrl: "angular_views/licniPodaci.html",
            controller: "homeController"
        })
        .when("/prisustvo", {
            templateUrl: "angular_views/prisustvo.html",
            controller: "prisustvoController"
        })
        .when("/ocjene", {
            templateUrl: "angular_views/ocjene.html",
            controller: "ocjeneController"
        })
        .when("/zadace", {
            templateUrl: "angular_views/zadace.html",
            controller: "zadaceController"
        })
        .when("/messenger", {
            templateUrl: "angular_views/messenger.html",
            controller: "messengerController"
        })
        .when("/login", {
            templateUrl: "angular_views/login.html",
            controller: "loginController"
        })
        .otherwise({ redirectTo: "/licniPodaci" });
});
