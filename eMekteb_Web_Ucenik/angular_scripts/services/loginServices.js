﻿(function () {
    "use strict";

    ucenikApp.factory("loginServices", ['$http', 'Config', function ($http, Config) {

        var sourceExtension = "Login/";

        var post = function (object) {
            return $http.post(Config.source + sourceExtension, object);
        };

        return {
            post: post
        };

    }]);

})();