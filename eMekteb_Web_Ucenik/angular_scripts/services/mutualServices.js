﻿(function () {
    "use strict";

    ucenikApp.factory("mutualServices", ['$http', 'Config', function ($http, Config) {

        var getUcenikByUsername = function (username) {
            return $http.get(Config.source + 'uceniks/GetUcenikByUsername/' + username);
        };
        
        var getOsobaByUsername = function (username) {
            return $http.get(Config.source + 'Osobas/GetOsobaByUsername/' + username);
        };

        var updateUcenik = function (osobaId, osoba) {
            return $http.put(Config.source + 'Osobas/' + osobaId, osoba);
        };

        var getAktivnostiByGodina = function (godina, isCurrent) {
            return $http.get(Config.source + "KalendarskaAktivnosts/GetAktivnostsByGodina/" + godina + "/" + isCurrent);
        };

        var getLekcijeByRazred = function (razred) {
            return $http.get(Config.source + "Lekcijas/GetByRazred/" + razred);
        };

        var getOcjene = function (polaznikId) {
            return $http.get(Config.source + "EvidencijaNapredovanjas/GetPolozeneLekcije/" + polaznikId);
        };

        var getPrisustvo = function (polaznikId) {
            return $http.get(Config.source + "Polazniks/GetPrisustvoByPolaznikId/" + polaznikId);
        };

        var getIzostanci = function (ucenikId) {
            return $http.get(Config.source + "Polazniks/GetIzostanci/" + ucenikId);
        };

        var getZadace = function (polaznikId) {
            return $http.get(Config.source + "Zadacas/GetZadaceByPolaznik/" + polaznikId);
        };

        // MESSENGER SERVICES

        var getAllConversations = function (osobaId) {
            return $http.get(Config.source + 'Porukes/GetAllConversations/' + osobaId);
        };

        var getMessagesFromConversation = function (osoba1Id, osoba2Id) {
            return $http.get(Config.source + 'Porukes/GetMessagesFromConversation/' + osoba1Id + '/' + osoba2Id);
        };

        var messagePost = function (object) {
            return $http.post(Config.source + 'Porukes/', object);
        };

        var getOsobasForConversation = function () {
            return $http.get(Config.source + 'Osobas/GetOsobasForConversation');
        };

        // END OF MESSENGER SERVICES

        return {
            getUcenikByUsername: getUcenikByUsername,
            getOsobaByUsername: getOsobaByUsername,
            updateUcenik:updateUcenik,
            getAktivnostiByGodina: getAktivnostiByGodina,
            getLekcijeByRazred: getLekcijeByRazred,
            getOcjene: getOcjene,
            getPrisustvo: getPrisustvo,
            getIzostanci: getIzostanci,
            getZadace: getZadace,

            // MESSENGER SERVICES
            getAllConversations: getAllConversations,
            getMessagesFromConversation: getMessagesFromConversation,
            messagePost: messagePost,
            getOsobasForConversation: getOsobasForConversation
            // END OF MESSENGER SERVICES

        };

    }]);

})();