﻿(function () {

    ucenikApp.controller("homeController", function ($q, $rootScope, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;
        var loggedUser = localStorageService.get('authorizationDataUcenik').loggedUser.data;

        var getBasicInfo = function (username) {
            $scope.isLoading = true;
            services.getUcenikByUsername(username).then(
                    function (response) {
                        $scope.ucenik = response.data;
                        $scope.isLoading = false;
                        localStorageService.set('ucenikModelData', { ucenik: response.data });
                        getLekcijeByRazred($scope.ucenik.razred.slice(-1));
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o učeniku nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getOsobaInfo = function (username) {
            $scope.isLoading = true;
            services.getOsobaByUsername(username).then(
                    function (response) {
                        $scope.osoba = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o učeniku nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getAktivnostiByGodina = function () {
            $scope.isLoading = true;
            services.getAktivnostiByGodina(1, true).then(
                    function (response) {
                        $scope.aktivnosti = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o aktivnostima nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getLekcijeByRazred = function (razred) {
            $scope.isLoading = true;
            services.getLekcijeByRazred(razred).then(
                    function (response) {
                        $scope.lekcije = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o lekcijama nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };
        
        $scope.updateUcenik = function () {
            services.updateUcenik($scope.osoba.id, $scope.osoba).then(
                    function (response) {
                        notificationsConfig.success("Podaci o učeniku su uspješno izmjenjeni!");
                        getBasicInfo(loggedUser.korisnickoIme);
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o učeniku nisu učitani!");
                    }
                );
        };

        $rootScope.activeTabAktivnosti = 'tab1';
        $rootScope.activeTabProfil = 'tab1';
        $scope.setActiveTab = function (activeId, context) {
            if (context == 'aktivnosti')
                return ($rootScope.activeTabAktivnosti == activeId) ? 'active' : '';
            else
                return ($rootScope.activeTabProfil == activeId) ? 'active' : '';
        };

        //get data on page load
        getBasicInfo(loggedUser.korisnickoIme);
        getOsobaInfo(loggedUser.korisnickoIme);
        getAktivnostiByGodina();

        $scope.mockedOpis =
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet dolore magna aliquam tincidunt erat volutpat.';

    });

}());

