﻿(function () {

    ucenikApp.controller("sidebarController", function ($location, $scope, localStorageService) {

        $scope.logout = function () {
            localStorageService.remove('authorizationDataUcenik');
            $location.path('/login');
        };

        //check if user logged in
        if (!localStorageService.get('authorizationDataUcenik'))
            $scope.logout();
        else {

            $scope.loggedUser = localStorageService.get('authorizationDataUcenik').loggedUser.data;
            $scope.name = $scope.loggedUser.ime + ' ' + $scope.loggedUser.prezime;

            $scope.getClass = function (path) {
                return ($location.path().substr(0, path.length) === path) ? 'active' : '';
            };

            $scope.openMessenger = function (id1, id2) {
                localStorageService.set('messengerData', { id1: id1, id2: id2 });
                $location.path('/messenger');
            };
        }

    });

}());

