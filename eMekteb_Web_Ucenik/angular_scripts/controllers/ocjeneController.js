﻿(function () {

    ucenikApp.controller("ocjeneController", function ($q, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;
        var loggedUser = localStorageService.get('authorizationDataUcenik').loggedUser.data;
        var ucenik = localStorageService.get('ucenikModelData').ucenik;

        var getOcjene = function (polaznikId) {
            $scope.isLoading = true;
            services.getOcjene(polaznikId).then(
                    function (response) {
                        $scope.lekcije = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o ocjenama nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getUcenikInfo = function (username) {
            $scope.isLoading = true;
            services.getUcenikByUsername(username).then(
                    function (response) {
                        ucenik = response.data;
                        $scope.isLoading = false;
                        getOcjene(ucenik.polaznikId);
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o učeniku nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        if (!ucenik)
            getUcenikInfo(loggedUser.korisnickoIme);
        else
            getOcjene(ucenik.polaznikId);

    });

}());

