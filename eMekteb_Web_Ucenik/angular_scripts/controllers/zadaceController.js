﻿(function () {

    ucenikApp.controller("zadaceController", function ($q, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;
        var loggedUser = localStorageService.get('authorizationDataUcenik').loggedUser.data;
        var ucenik = localStorageService.get('ucenikModelData').ucenik;

        //CRUD functions
        var getZadace = function (polaznikId) {
            $scope.isLoading = true;
            services.getZadace(polaznikId).then(
                    function (response) {
                        $scope.zadace = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o zadaćama nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getUcenikInfo = function (username) {
            $scope.isLoading = true;
            services.getUcenikByUsername(username).then(
                    function (response) {
                        ucenik = response.data;
                        $scope.isLoading = false;
                        getZadace(ucenik.polaznikId);
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o učeniku nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        if (!ucenik)
            getUcenikInfo(loggedUser.korisnickoIme);
        else {
            getZadace(ucenik.polaznikId);
        }

    });

}());

