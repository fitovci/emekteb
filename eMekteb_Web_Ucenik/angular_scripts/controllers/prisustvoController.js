﻿(function () {

    ucenikApp.controller("prisustvoController", function ($q, $scope, $filter, localStorageService, mutualServices) {

        $scope.isLoading = true;
        var services = mutualServices;
        var loggedUser = localStorageService.get('authorizationDataUcenik').loggedUser.data;
        var ucenik = localStorageService.get('ucenikModelData').ucenik;

        //CRUD functions
        var getPrisustvo = function (polaznikId) {
            $scope.isLoading = true;
            services.getPrisustvo(polaznikId).then(
                    function (response) {
                        $scope.prisustva = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o prisustvu nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getIzostanci = function (ucenikId) {
            $scope.isLoading = true;
            services.getIzostanci(ucenikId).then(
                    function (response) {
                        $scope.izostanci = response.data;
                        $scope.isLoading = false;
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o izostancma nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        var getUcenikInfo = function (username) {
            $scope.isLoading = true;
            services.getUcenikByUsername(username).then(
                    function (response) {
                        ucenik = response.data;
                        $scope.isLoading = false;
                        getPrisustvo(ucenik.polaznikId);
                        getIzostanci(ucenik.id);
                    },
                    function () {
                        notificationsConfig.error("Desila se greška. Podaci o prisustvu nisu učitani!");
                        $scope.isLoading = false;
                    }
                );
        };

        if (!ucenik)
            getUcenikInfo(loggedUser.korisnickoIme);
        else {
            getIzostanci(ucenik.ucenikId);
            getPrisustvo(ucenik.polaznikId);
        }

    });

}());

